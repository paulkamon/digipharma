package groupdigital.com.paiementcie.Alert;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import groupdigital.com.paiementcie.Activities.flow.EntrezMontantActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.ResponseServer;
import groupdigital.com.paiementcie.R;

public class CallbackMobileMoneyActivity extends AppCompatActivity {

    private Timer timer;
    private  String notrans;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callback_mobile_money);
        intent = getIntent();
        notrans  = intent.getStringExtra("notrans");
        final String url = "http://178.33.227.9:2050/v0.1/paiements/check_statut/"+notrans;
        final Handler handler = new Handler();
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            PerformBackgroundTask performBackgroundTask = new PerformBackgroundTask();
                            // PerformBackgroundTask this class is the class that extends AsynchTask
                            performBackgroundTask.execute(url);

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 5000);

    }

    public class PerformBackgroundTask extends AsyncTask<String, Void, String> {
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 100000;
        public static final int CONNECTION_TIMEOUT = 100000;
        @Override
        protected String doInBackground(String... params){
            String stringUrl = params[0];
            String result;
            String inputLine;
            try {
                //Create a URL object holding our url
                URL myUrl = new URL(stringUrl);
                //Create a connection
                HttpURLConnection connection =(HttpURLConnection)
                        myUrl.openConnection();
                //Set methods and timeouts
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                //Connect to our url
                connection.connect();
                //Create a new InputStreamReader
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                //Create a new buffered reader and String Builder
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                //Check if the line we are reading is not null
                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }
                //Close our InputStream and Buffered reader
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
            }
            catch(IOException e){
                e.printStackTrace();
                result = null;
            }
            return result;
        }
        protected void onPostExecute(String result){
            super.onPostExecute(result);

            ResponseServer responseServer=null;
            Log.i("RETOUR SERVEUR",result.toString());
            try {
                JSONObject jsonObject = new JSONObject(result.toString());
                responseServer = ResponseServer.fromJson(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(responseServer!=null && responseServer.getStatus()==true){
                Log.i("RETOUR SERVEUR",result.toString());
                timer.cancel();
                AlertDialog.Builder builder = new AlertDialog.Builder(CallbackMobileMoneyActivity.this);
                builder.setMessage( responseServer.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                finish();
                                AutoLoad.startNewActivity(CallbackMobileMoneyActivity.this, EntrezMontantActivity.class);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }else if(responseServer!=null){
                Log.i("RETOUR MESSAGE",responseServer.getMessage());
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
