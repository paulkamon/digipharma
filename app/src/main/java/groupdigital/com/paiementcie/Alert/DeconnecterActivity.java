package groupdigital.com.paiementcie.Alert;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import groupdigital.com.paiementcie.Activities.home.TaxCollectActivity;
import groupdigital.com.paiementcie.R;

public class DeconnecterActivity extends AppCompatActivity {

    private Typeface Light, Bold;
    private TextView titleEmail, textMsg;
    private Button btnYes, btnNo;
    private Intent intent;
    private String message;
    private int key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_deconnecter);

        intent = getIntent();
        key  = intent.getIntExtra("key", 0);
        message  = intent.getStringExtra("msg");


        titleEmail = (TextView) findViewById(R.id.titleEmail);
        textMsg = (TextView) findViewById(R.id.textMsg);
        btnYes = (Button) findViewById(R.id.btnYes);
        btnNo = (Button) findViewById(R.id.btnNo);

        Light = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        Bold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");

        textMsg.setText(message);
        textMsg.setTypeface(Light);
        titleEmail.setTypeface(Bold);
        btnYes.setTypeface(Bold);
        btnNo.setTypeface(Bold);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actuator();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void actuator() {
        if(key == 1){
            sendIntent("logout");
            finish();
        }
    }

    private void sendIntent(String type){
        Intent intents = new Intent(TaxCollectActivity.MESSAGE_PROGRESS);
        intents.putExtra("typeMsg", "logout");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intents);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
