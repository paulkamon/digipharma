package groupdigital.com.paiementcie.Alert;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import groupdigital.com.paiementcie.Activities.impression.ImprimerActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Utilitaire.Constants;

public class AlertActivity extends AppCompatActivity {

    private Typeface Light, Bold;
    private TextView titleEmail, textMsg;
    private Button btnOK;
    private Intent intent;
    private String message, key;
    private int numeroKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);

        intent = getIntent();
        message  = intent.getStringExtra("msg");
        key  = intent.getStringExtra("key");

        titleEmail = (TextView) findViewById(R.id.titleEmail);
        textMsg = (TextView) findViewById(R.id.textMsg);
        btnOK = (Button) findViewById(R.id._ok);

        fontPolice();
    }

    private void fontPolice() {
        Light = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        Bold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");

        changeStyle();
    }

    private void changeStyle() {
        textMsg.setText(message);
        textMsg.setTypeface(Light);
        titleEmail.setTypeface(Bold);
        btnOK.setTypeface(Bold);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionClick();
            }
        });
    }

    private void actionClick() {
        if(key.equals("close")){
            finish();
        }
        else if(key.equals("died")){
            finish();
            System.exit(0);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
