package groupdigital.com.paiementcie.Alert;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import groupdigital.com.paiementcie.Activities.home.TaxCollectActivity;
import groupdigital.com.paiementcie.Activities.impression.ImprimerActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Utilitaire.Constants;

import java.util.ArrayList;
import java.util.List;

public class CongratulationActivity extends AppCompatActivity {

    private Typeface Light, Bold, RobotoMedium;
    private TextView titl1, comment, titreHeader;
    private Button btnImprimer, goAtHome;
    private Intent intent;
    private String numRecu, typePay;
    private UserInformationDto userDto;
    private List<FactureDto> factureDto;
    private PayFactureDto listFacture;
    private int mRestant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_congratulation);

        boolean paulImprime = AutoLoad.getPrefs().getBoolean(Constants.PREF_PRINT, false);

        intent = getIntent();
        numRecu  = intent.getStringExtra("nRecu");
        typePay  = intent.getStringExtra("typePay");
        mRestant  = intent.getIntExtra("rendueSom", 0);
        userDto  = (UserInformationDto) intent.getSerializableExtra("userDto");

        if(typePay.equals("Card") || typePay.equals("Momo") || typePay.equals("cb")){
            factureDto  = (List<FactureDto>)  intent.getSerializableExtra("info");
        }
        else if(typePay.equals("cash") || typePay.equals("cheque")) {
            listFacture  = (PayFactureDto) intent.getSerializableExtra("info");
        }
        titreHeader = (TextView) findViewById(R.id.titreHeader);
        titl1 = (TextView) findViewById(R.id.titl1);
        comment = (TextView) findViewById(R.id.comment);
        btnImprimer = (Button) findViewById(R.id.printBtn);
        goAtHome = (Button) findViewById(R.id.goAtHome);

        //Recupératon de la pref print
        if(paulImprime){
            btnImprimer.setVisibility(View.VISIBLE);
        }else{
            goAtHome.setVisibility(View.VISIBLE);
        }

        fontPolice();
    }

    private void fontPolice() {
        RobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        Light = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        Bold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");

        changeStyle();
    }

    private void changeStyle() {
        comment.setTypeface(Light);
        titreHeader.setTypeface(RobotoMedium);
        titl1.setTypeface(Bold);
        btnImprimer.setTypeface(Bold);
        goAtHome.setTypeface(Bold);

        btnImprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImpressionRecu();
            }
        });

        goAtHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutoLoad.startNewActivity(CongratulationActivity.this, TaxCollectActivity.class);
                finish();
            }
        });
    }

    private void ImpressionRecu() {
        intent = new Intent(this, ImprimerActivity.class);
        intent.putExtra("numRecu", numRecu);
        intent.putExtra("userDto", userDto);
        intent.putExtra("mRestant", mRestant);
        intent.putExtra("factureDto", (ArrayList<FactureDto>) factureDto);
        intent.putExtra("payFacture", listFacture);
        intent.putExtra("typePay", typePay);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
