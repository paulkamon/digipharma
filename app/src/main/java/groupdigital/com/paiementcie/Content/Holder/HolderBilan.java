package groupdigital.com.paiementcie.Content.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import groupdigital.com.paiementcie.R;

/**
 * Created by digital on 26/11/2017.
 */

public class HolderBilan extends RecyclerView.ViewHolder {

    public TextView nomClient, montantPreleve, modePaiement;

    public HolderBilan(View itemView) {
        super(itemView);

        nomClient = (TextView) itemView.findViewById(R.id.nomClient);
        montantPreleve = (TextView) itemView.findViewById(R.id.montantPreleve);
        modePaiement = (TextView) itemView.findViewById(R.id.modePaiement);
    }
}
