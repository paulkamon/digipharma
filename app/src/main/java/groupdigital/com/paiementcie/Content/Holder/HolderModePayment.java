package groupdigital.com.paiementcie.Content.Holder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ViewClickListener;

/**
 * Created by digital on 26/11/2017.
 */

public class HolderModePayment extends RecyclerView.ViewHolder {

    public RelativeLayout realiPayment;
    public TextView paymentLabel;

    public HolderModePayment(View itemView, final ViewClickListener vlistener) {
        super(itemView);
        realiPayment = (RelativeLayout) itemView.findViewById(R.id.realiPayment);
        paymentLabel = (TextView) itemView.findViewById(R.id.paymentLabel);

        realiPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vlistener != null){
                    vlistener.onClicItemWithParam((Integer) view.getTag(), "token");
                }
            }
        });
    }
}
