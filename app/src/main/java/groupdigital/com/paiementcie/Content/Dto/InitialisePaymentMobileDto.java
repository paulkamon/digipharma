package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by digital on 27/11/2017.
 */

public class InitialisePaymentMobileDto implements Serializable {

    private String msisdn;
    private long  montant;
    private String canal;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public long getMontant() {
        return montant;
    }

    public void setMontant(long montant) {
        this.montant = montant;
    }

    @Override
    public String toString() {
        return "InitialisePaymentMobileDto{" +
                "msisdn='" + msisdn + '\'' +

                ", montant=" + montant +
                ", canal='" + canal + '\'' +
                '}';
    }
}
