package groupdigital.com.paiementcie.Content.Holder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ViewClickListener;

/**
 * Created by digital on 26/11/2017.
 */

public class HolderFacture extends RecyclerView.ViewHolder {

    public CheckBox invoiceCheckBox;

    public HolderFacture(View itemView, final ViewClickListener vlistener) {
        super(itemView);
        invoiceCheckBox = (CheckBox) itemView.findViewById(R.id.invoice);

        invoiceCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(vlistener != null){
                if (invoiceCheckBox.isChecked()){
                    vlistener.onClicItemWithParam((Integer) view.getTag(), "plus");
                }
                else{
                    vlistener.onClicItemWithParam((Integer) view.getTag(), "moins");
                }
            }
            }
        });
    }
}
