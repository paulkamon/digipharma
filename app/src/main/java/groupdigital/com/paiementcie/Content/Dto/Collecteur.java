package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by digital on 25/11/2017.
 */

public class Collecteur implements Serializable {

    private String username,password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
