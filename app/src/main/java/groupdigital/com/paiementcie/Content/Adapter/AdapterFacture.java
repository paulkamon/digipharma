package groupdigital.com.paiementcie.Content.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Holder.HolderFacture;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ViewClickListener;

import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class AdapterFacture extends RecyclerView.Adapter<HolderFacture> {

    private Context contextAdapter;
    private List<FactureDto> factureDtoList;
    private View viewAdapter;
    private Typeface RobotoLight;
    private ViewClickListener mListener;

    public AdapterFacture(Context contextAdapter, List<FactureDto> factureDtoList, ViewClickListener mListener) {
        this.contextAdapter = contextAdapter;
        this.factureDtoList = factureDtoList;
        this.mListener = mListener;
    }

    @Override
    public HolderFacture onCreateViewHolder(ViewGroup parent, int viewType) {
        viewAdapter = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_facture_impaye, parent, false);
        return new HolderFacture(viewAdapter, mListener);
    }

    @Override
    public void onBindViewHolder(final HolderFacture holder, final int position) {
        RobotoLight = Typeface.createFromAsset(contextAdapter.getAssets(), "fonts/Roboto-Light.ttf");
        final FactureDto model = factureDtoList.get(position);
        holder.invoiceCheckBox.setText(contextAdapter.getResources().getString(R.string.invoice)+" n°"
                +model.getNumero()+" | "
                +model.getDateFacture()+ " | " +
                model.getMontant() + " "
                +contextAdapter.getResources().getString(R.string.devise));
        holder.invoiceCheckBox.setTag(position);
        holder.invoiceCheckBox.setTypeface(RobotoLight);
        /*if (position == 0){
            //Log.i("OK--Position", String.valueOf(position));
        }
        else{
            int pos = position-1;
            final FactureDto model = factureDtoList.get(pos);
            holder.invoiceCheckBox.setText("Invoice "+model.getDateFacture()+ " - " + model.getMontant() + " GHS");
            holder.invoiceCheckBox.setTag(pos);
            holder.invoiceCheckBox.setTypeface(RobotoLight);
        }*/
    }

    @Override
    public int getItemCount() {
        return factureDtoList.size();
        //return factureDtoList.size()+1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface ClickListener
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener
    {
        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
