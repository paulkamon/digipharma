package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 27/11/2017.
 */

public class BankCardDto implements Serializable {
    private long getOperatorId,idCollector, idContribuable, idModePaiement, amount;
    private List<FactureDto> factureDto;
    private String numeroCarteBancaire, bankName, validityDate, codecard, pin, numeroMobile, username;

    public long getGetOperatorId() {
        return getOperatorId;
    }

    public void setGetOperatorId(long getOperatorId) {
        this.getOperatorId = getOperatorId;
    }

    public long getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(long idCollector) {
        this.idCollector = idCollector;
    }

    public long getIdContribuable() {
        return idContribuable;
    }

    public void setIdContribuable(long idContribuable) {
        this.idContribuable = idContribuable;
    }

    public long getIdModePaiement() {
        return idModePaiement;
    }

    public void setIdModePaiement(long idModePaiement) {
        this.idModePaiement = idModePaiement;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public List<FactureDto> getFactureDto() {
        return factureDto;
    }

    public void setFactureDto(List<FactureDto> factureDto) {
        this.factureDto = factureDto;
    }

    public String getNumeroCarteBancaire() {
        return numeroCarteBancaire;
    }

    public void setNumeroCarteBancaire(String numeroCarteBancaire) {
        this.numeroCarteBancaire = numeroCarteBancaire;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(String validityDate) {
        this.validityDate = validityDate;
    }

    public String getCodecard() {
        return codecard;
    }

    public void setCodecard(String codecard) {
        this.codecard = codecard;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getNumeroMobile() {
        return numeroMobile;
    }

    public void setNumeroMobile(String numeroMobile) {
        this.numeroMobile = numeroMobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
