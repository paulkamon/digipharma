package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 27/11/2017.
 */

public class PayFactureDto implements Serializable {

    private long idCollector, idContribuable, idModePaiement;
    private String numeroCheque, username;
    private int montantFacture, sommeRecu, sommeRestante;
    private List<FactureDto> factureDto;

    public long getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(long idCollector) {
        this.idCollector = idCollector;
    }

    public long getIdContribuable() {
        return idContribuable;
    }

    public void setIdContribuable(long idContribuable) {
        this.idContribuable = idContribuable;
    }

    public long getIdModePaiement() {
        return idModePaiement;
    }

    public void setIdModePaiement(long idModePaiement) {
        this.idModePaiement = idModePaiement;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public List<FactureDto> getFactureDto() {
        return factureDto;
    }

    public void setFactureDto(List<FactureDto> factureDto) {
        this.factureDto = factureDto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getMontantFacture() {
        return montantFacture;
    }

    public void setMontantFacture(int montantFacture) {
        this.montantFacture = montantFacture;
    }

    public int getSommeRecu() {
        return sommeRecu;
    }

    public void setSommeRecu(int sommeRecu) {
        this.sommeRecu = sommeRecu;
    }

    public int getSommeRestante() {
        return sommeRestante;
    }

    public void setSommeRestante(int sommeRestante) {
        this.sommeRestante = sommeRestante;
    }

    @Override
    public String toString() {
        return "PayFactureDto{" +
                "idCollector=" + idCollector +
                ", idContribuable=" + idContribuable +
                ", idModePaiement=" + idModePaiement +
                ", numeroCheque='" + numeroCheque + '\'' +
                ", username='" + username + '\'' +
                ", factureDto=" + factureDto +
                '}';
    }
}
