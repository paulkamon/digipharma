package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by digital on 26/11/2017.
 */

public class FactureDto implements Serializable {

    private int id, montant;
    private String dateFacture, numero;

    public FactureDto(int montant, String dateFacture, String numero) {
        this.montant = montant;
        this.dateFacture = dateFacture;
        this.numero = numero;
    }

    public FactureDto(int id, int montant, String dateFacture, String numero) {
        this.id = id;
        this.montant = montant;
        this.dateFacture = dateFacture;
        this.numero = numero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(String dateFacture) {
        this.dateFacture = dateFacture;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FactureDto that = (FactureDto) o;

        return id == that.id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "FactureDto{" +
                "id=" + id +
                ", montant=" + montant +
                ", dateFacture='" + dateFacture + '\'' +
                '}';
    }
}
