package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by digital on 26/11/2017.
 */

public class ModePaymentDto implements Serializable {

    private int id;
    private String modePaiement;

    public ModePaymentDto(int id, String modePaiement) {
        this.id = id;
        this.modePaiement = modePaiement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }
}
