package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by aquer on 19/01/2018.
 */

public class AddressDto implements Serializable {

    private double longitude, latitude;
    private String addressLine;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }


    @Override
    public String toString() {
        return "AddressDto{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", addressLine='" + addressLine + '\'' +
                '}';
    }
}