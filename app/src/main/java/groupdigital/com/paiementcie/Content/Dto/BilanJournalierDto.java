package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by digital on 26/11/2017.
 */

public class BilanJournalierDto implements Serializable{

    private long id, montantPreleve;
    private String nom, modePaiement;

    public BilanJournalierDto(long montantPreleve, String nom, String modePaiement) {
        this.montantPreleve = montantPreleve;
        this.nom = nom;
        this.modePaiement = modePaiement;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMontantPreleve() {
        return montantPreleve;
    }

    public void setMontantPreleve(long montantPreleve) {
        this.montantPreleve = montantPreleve;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    @Override
    public String toString() {
        return "BilanJournalierDto{" +
                "id=" + id +
                ", montantPreleve=" + montantPreleve +
                ", nom='" + nom + '\'' +
                ", modePaiement='" + modePaiement + '\'' +
                '}';
    }
}
