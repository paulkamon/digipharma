package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 27/11/2017.
 */

public class BankPaymentDto implements Serializable {

    private String otp, username, compteBancaire;
    private long getOperatorId,idCollector, idContribuable, idModePaiement, amount;
    private List<FactureDto> factureDto;

    //private PayFactureDto payFactureDto;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public long getGetOperatorId() {
        return getOperatorId;
    }

    public void setGetOperatorId(long getOperatorId) {
        this.getOperatorId = getOperatorId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(long idCollector) {
        this.idCollector = idCollector;
    }

    public long getIdContribuable() {
        return idContribuable;
    }

    public void setIdContribuable(long idContribuable) {
        this.idContribuable = idContribuable;
    }

    public long getIdModePaiement() {
        return idModePaiement;
    }

    public void setIdModePaiement(long idModePaiement) {
        this.idModePaiement = idModePaiement;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<FactureDto> getFactureDto() {
        return factureDto;
    }

    public void setFactureDto(List<FactureDto> factureDto) {
        this.factureDto = factureDto;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }
}
