package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 27/11/2017.
 */

public class MobilePaymentDto implements Serializable {

    private String otp,numeroMobile, numeroCheque, username;
    private long getOperatorId, amount,idCollector, idContribuable, idModePaiement;
    private List<FactureDto> factureDto;

    //private PayFactureDto payFactureDto;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getNumeroMobile() {
        return numeroMobile;
    }

    public void setNumeroMobile(String numeroMobile) {
        this.numeroMobile = numeroMobile;
    }

    public long getGetOperatorId() {
        return getOperatorId;
    }

    public void setGetOperatorId(long getOperatorId) {
        this.getOperatorId = getOperatorId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(long idCollector) {
        this.idCollector = idCollector;
    }

    public long getIdContribuable() {
        return idContribuable;
    }

    public void setIdContribuable(long idContribuable) {
        this.idContribuable = idContribuable;
    }

    public long getIdModePaiement() {
        return idModePaiement;
    }

    public void setIdModePaiement(long idModePaiement) {
        this.idModePaiement = idModePaiement;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<FactureDto> getFactureDto() {
        return factureDto;
    }

    public void setFactureDto(List<FactureDto> factureDto) {
        this.factureDto = factureDto;
    }
}
