package groupdigital.com.paiementcie.Content.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import groupdigital.com.paiementcie.Content.Dto.ModePaymentDto;
import groupdigital.com.paiementcie.Content.Holder.HolderModePayment;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ViewClickListener;

import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class AdapterModePayment extends RecyclerView.Adapter<HolderModePayment> {

    private Context contextAdapter;
    private List<ModePaymentDto> modePaymentDtoList;
    private View viewAdapter;
    private Typeface RobotoBlack;
    private ViewClickListener mListener;

    public AdapterModePayment(Context contextAdapter, List<ModePaymentDto> modePaymentDtoList, ViewClickListener mListener) {
        this.contextAdapter = contextAdapter;
        this.modePaymentDtoList = modePaymentDtoList;
        this.mListener = mListener;
    }

    @Override
    public HolderModePayment onCreateViewHolder(ViewGroup parent, int viewType) {
        viewAdapter = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_mode, parent, false);
        return new HolderModePayment(viewAdapter, mListener);
    }

    @Override
    public void onBindViewHolder(HolderModePayment holder, int position) {
        RobotoBlack = Typeface.createFromAsset(contextAdapter.getAssets(), "fonts/Roboto-Black.ttf");

        final ModePaymentDto model = modePaymentDtoList.get(position);
        holder.paymentLabel.setText(model.getModePaiement());
        holder.realiPayment.setTag(position);

        holder.paymentLabel.setTypeface(RobotoBlack);
    }

    @Override
    public int getItemCount() {
        return modePaymentDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface ClickListener
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener
    {
        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
