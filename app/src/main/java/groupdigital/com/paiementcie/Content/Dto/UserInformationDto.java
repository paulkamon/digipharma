package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class UserInformationDto implements Serializable {

    private String nom, prenoms, age, contact, photo, montant, numero, dateNaissance;
    private List<FactureDto> factureDto;
    private long id;
    private double latitude, longitude;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public List<FactureDto> getFactureDto() {
        return factureDto;
    }

    public void setFactureDto(List<FactureDto> factureDto) {
        this.factureDto = factureDto;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "UserInformationDto{" +
                "nom='" + nom + '\'' +
                ", prenoms='" + prenoms + '\'' +
                ", age='" + age + '\'' +
                ", contact='" + contact + '\'' +
                ", photo='" + photo + '\'' +
                ", montant='" + montant + '\'' +
                ", numero='" + numero + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                ", factureDto=" + factureDto +
                ", id=" + id +
                '}';
    }
}
