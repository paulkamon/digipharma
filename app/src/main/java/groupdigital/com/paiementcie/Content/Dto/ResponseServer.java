package groupdigital.com.paiementcie.Content.Dto;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseServer {
    private boolean status;
    private String message;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static ResponseServer fromJson(JSONObject jsonObject) {
        ResponseServer responseServer = new ResponseServer();
        // Deserialize json into object fields
        try {
            responseServer.status = jsonObject.getBoolean("status");
            responseServer.message = jsonObject.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        // Return new object
        return responseServer;
    }
}
