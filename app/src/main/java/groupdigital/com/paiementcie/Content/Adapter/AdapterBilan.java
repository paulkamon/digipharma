package groupdigital.com.paiementcie.Content.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import groupdigital.com.paiementcie.Content.Dto.BilanJournalierDto;
import groupdigital.com.paiementcie.Content.Holder.HolderBilan;
import groupdigital.com.paiementcie.R;

import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class AdapterBilan extends RecyclerView.Adapter<HolderBilan> {

    private Context contextAdapter;
    private List<BilanJournalierDto> bilanJournalierDtoList;
    private View viewAdapter;
    private Typeface RobotoLight;

    public AdapterBilan(Context contextAdapter, List<BilanJournalierDto> bilanJournalierDtoList) {
        this.contextAdapter = contextAdapter;
        this.bilanJournalierDtoList = bilanJournalierDtoList;
    }

    public List<BilanJournalierDto> getBilanJournalierDtoList() {
        return bilanJournalierDtoList;
    }

    public void setBilanJournalierDtoList(List<BilanJournalierDto> bilanJournalierDtoList) {
        this.bilanJournalierDtoList = bilanJournalierDtoList;
    }

    @Override
    public HolderBilan onCreateViewHolder(ViewGroup parent, int viewType) {
        viewAdapter = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_releve, parent, false);
        return new HolderBilan(viewAdapter);
    }

    @Override
    public void onBindViewHolder(HolderBilan holder, int position) {
        RobotoLight = Typeface.createFromAsset(contextAdapter.getAssets(), "fonts/Roboto-Light.ttf");

        final BilanJournalierDto model = bilanJournalierDtoList.get(position);
        holder.nomClient.setText(model.getNom());
        holder.montantPreleve.setText(""+model.getMontantPreleve()+"");
        holder.modePaiement.setText(model.getModePaiement());

        holder.nomClient.setTypeface(RobotoLight);
        holder.montantPreleve.setTypeface(RobotoLight);
        holder.modePaiement.setTypeface(RobotoLight);
    }

    @Override
    public int getItemCount() {
        return bilanJournalierDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface ClickListener
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener
    {
        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
