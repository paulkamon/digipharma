package groupdigital.com.paiementcie.Content.Dto;

import java.io.Serializable;

/**
 * Created by digital on 27/11/2017.
 */

public class OtpDto implements Serializable {

    private boolean status;
    private String code, object, response;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
