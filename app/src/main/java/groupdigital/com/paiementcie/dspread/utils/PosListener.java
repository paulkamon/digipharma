package groupdigital.com.paiementcie.dspread.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dspread.xpos.QPOSService;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.dspread.DSPREADMain;


class PosListener implements QPOSService.QPOSServiceListener {

    private static final String TAG = "PosListener";

    @Override
    public void onRequestWaitingUser() {

    }

    @Override
    public void onQposIdResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onQposKsnResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onQposIsCardExist(boolean b) {

    }

    @Override
    public void onRequestDeviceScanFinished() {

    }

    @Override
    public void onQposInfoResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onQposGenerateSessionKeysResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onQposDoSetRsaPublicKey(boolean b) {

    }

    @Override
    public void onSearchMifareCardResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onDoTradeResult(QPOSService.DoTradeResult doTradeResult, Hashtable<String, String> hashtable) {

    }

    @Override
    public void onFinishMifareCardResult(boolean b) {

    }

    @Override
    public void onVerifyMifareCardResult(boolean b) {

    }

    @Override
    public void onReadMifareCardResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onWriteMifareCardResult(boolean b) {

    }

    @Override
    public void onOperateMifareCardResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void getMifareCardVersion(Hashtable<String, String> hashtable) {

    }

    @Override
    public void getMifareReadData(Hashtable<String, String> hashtable) {

    }

    @Override
    public void getMifareFastReadData(Hashtable<String, String> hashtable) {

    }

    @Override
    public void writeMifareULData(String s) {

    }

    @Override
    public void verifyMifareULData(Hashtable<String, String> hashtable) {

    }

    @Override
    public void transferMifareData(String s) {

    }

    @Override
    public void onRequestSetAmount() {

    }

    @Override
    public void onRequestSelectEmvApp(ArrayList<String> arrayList) {

    }

    @Override
    public void onRequestIsServerConnected() {

    }

    @Override
    public void onRequestFinalConfirm() {

    }

    @Override
    public void onRequestOnlineProcess(String s) {

    }

    @Override
    public void onRequestTime() {

    }

    @Override
    public void onRequestTransactionResult(QPOSService.TransactionResult transactionResult) {

    }

    @Override
    public void onRequestTransactionLog(String s) {

    }

    @Override
    public void onRequestBatchData(String s) {

    }

    @Override
    public void onRequestQposConnected() {

    }

    @Override
    public void onRequestQposDisconnected() {

    }

    @Override
    public void onRequestNoQposDetected() {

    }

    @Override
    public void onRequestNoQposDetectedUnbond() {

    }

    @Override
    public void onError(QPOSService.Error error) {

    }

    @Override
    public void onRequestDisplay(QPOSService.Display display) {

    }

    @Override
    public void onReturnReversalData(String s) {

    }

    @Override
    public void onReturnGetPinResult(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onReturnPowerOnIccResult(boolean b, String s, String s1, int i) {

    }

    @Override
    public void onReturnPowerOffIccResult(boolean b) {

    }

    @Override
    public void onReturnApduResult(boolean b, String s, int i) {

    }

    @Override
    public void onReturnSetSleepTimeResult(boolean b) {

    }

    @Override
    public void onGetCardNoResult(String s) {

    }

    @Override
    public void onRequestSignatureResult(byte[] bytes) {

    }

    @Override
    public void onRequestCalculateMac(String s) {

    }

    @Override
    public void onRequestUpdateWorkKeyResult(QPOSService.UpdateInformationResult updateInformationResult) {

    }

    @Override
    public void onReturnCustomConfigResult(boolean b, String s) {

    }

    @Override
    public void onRequestSetPin() {

    }

    @Override
    public void onReturnSetMasterKeyResult(boolean b) {

    }

    @Override
    public void onRequestUpdateKey(String s) {

    }

    @Override
    public void onReturnUpdateIPEKResult(boolean b) {

    }

    @Override
    public void onReturnRSAResult(String s) {

    }

    @Override
    public void onReturnUpdateEMVResult(boolean b) {

    }

    @Override
    public void onReturnGetQuickEmvResult(boolean b) {

    }

    @Override
    public void onReturnGetEMVListResult(String s) {

    }

    @Override
    public void onReturnUpdateEMVRIDResult(boolean b) {

    }

    @Override
    public void onDeviceFound(BluetoothDevice bluetoothDevice) {

    }

    @Override
    public void onReturnBatchSendAPDUResult(LinkedHashMap<Integer, String> linkedHashMap) {

    }

    @Override
    public void onBluetoothBonding() {

    }

    @Override
    public void onBluetoothBonded() {

    }

    @Override
    public void onWaitingforData(String s) {

    }

    @Override
    public void onBluetoothBondFailed() {

    }

    @Override
    public void onBluetoothBondTimeout() {

    }

    @Override
    public void onReturniccCashBack(Hashtable<String, String> hashtable) {

    }

    @Override
    public void onLcdShowCustomDisplay(boolean b) {

    }

    @Override
    public void onUpdatePosFirmwareResult(QPOSService.UpdateInformationResult updateInformationResult) {

    }

    @Override
    public void onBluetoothBoardStateResult(boolean b) {

    }

    @Override
    public void onReturnDownloadRsaPublicKey(HashMap<String, String> hashMap) {

    }

    @Override
    public void onGetPosComm(int i, String s, String s1) {

    }

    @Override
    public void onUpdateMasterKeyResult(boolean b, Hashtable<String, String> hashtable) {

    }

    @Override
    public void onPinKey_TDES_Result(String s) {

    }

    @Override
    public void onEmvICCExceptionData(String s) {

    }

    @Override
    public void onSetParamsResult(boolean b, Hashtable<String, Object> hashtable) {

    }

    @Override
    public void onGetInputAmountResult(boolean b, String s) {

    }

    @Override
    public void onReturnNFCApduResult(boolean b, String s, int i) {

    }

    @Override
    public void onReturnPowerOnNFCResult(boolean b, String s, String s1, int i) {

    }

    @Override
    public void onReturnPowerOffNFCResult(boolean b) {

    }

    @Override
    public void onCbcMacResult(String s) {

    }

    @Override
    public void onReadBusinessCardResult(boolean b, String s) {

    }

    @Override
    public void onWriteBusinessCardResult(boolean b) {

    }

    @Override
    public void onConfirmAmountResult(boolean b) {

    }

    @Override
    public void onSetManagementKey(boolean b) {

    }

    @Override
    public void onSetSleepModeTime(boolean b) {

    }

    @Override
    public void onGetSleepModeTime(String s) {

    }

    @Override
    public void onGetShutDownTime(String s) {

    }

    @Override
    public void onEncryptData(String s) {

    }

    @Override
    public void onAddKey(boolean b) {

    }

    @Override
    public void onSetBuzzerResult(boolean b) {

    }

    @Override
    public void onSetBuzzerTimeResult(boolean b) {

    }

    @Override
    public void onSetBuzzerStatusResult(boolean b) {

    }

    @Override
    public void onGetBuzzerStatusResult(String s) {

    }

    @Override
    public void onQposDoTradeLog(boolean b) {

    }

    @Override
    public void onQposDoGetTradeLogNum(String s) {

    }

    @Override
    public void onQposDoGetTradeLog(String s, String s1) {

    }
}