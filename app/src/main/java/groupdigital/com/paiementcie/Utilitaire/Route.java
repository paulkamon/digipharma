package groupdigital.com.paiementcie.Utilitaire;

/**
 * Created by digital on 07/05/2017.
 */

public class Route {
    //public static final String BASE = "http://178.33.227.9:44380/";
    public static final String BASE = "http://178.33.227.9:44398/";
    public static final String IMAGE_PATH = BASE+"taxcollect/profilepicture";
    public static final String CONECTED_ACCOUNT = "api/collector/authenticate";
    public static final String BILAN_JOURNALIER = "api/reglement/collectbilan";
    public static final String FIND_CONTRIBUABLE = "api/contribuable/findcontribuable";
    public static final String PAYMENT_MODE = "api/moyenpaiement/getaveragepayement";
    public static final String PAYMENT_FACTURE_CASH_CHEQUE = "api/payement/payfacture";
    public static final String OPERATOR_LIST = "api/moyenpaiement/getoperatorpayement";
   // public static final String INITIALISE_PAYMENT = "api/payement/mobilemoneyinit";
   public static final String INITIALISE_PAYMENT = "http://178.33.227.9:2050/v0.1/paiements/mobile_money";
    public static final String FINALISE_PAYMENT_BAY_MOMO = "api/payement/mobilemoneycomplete";
    //public static final String FINALISE_PAYMENT_BAY_MOMO = "http://192.168.1.163:3000";
    public static final String FINALISE_PAYMENT_BY_CARD_BANK = "api/payement/bankcardcomplete";
    public static final String INITIALISE_PAYMENT_BANK = "api/payement/accountbankinit";
    public static final String FINISH_PAYMENT_BANK = "api/payement/accountbankcomplete";
    public static final String UPDATE_POSITION = "api/contribuable/updategps";
}
