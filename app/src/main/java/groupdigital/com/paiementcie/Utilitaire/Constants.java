package groupdigital.com.paiementcie.Utilitaire;


import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;


public class Constants {

    public static final String PREF_NAME = "Users";
    public static final String PREF_PRINT = "Print";
    public static final String PREF_NFC = "NFC";
    public static final String PREF_SMARTCART = "SmartCard";
    public static final String PREF_CONFIG = "settings";
    public static final int PREFS_MODE = Context.MODE_PRIVATE;

    private static SimpleDateFormat mDateFormat;
    public static final int TIME_NETWORK_RESPONSE = 120; //20


    public static String splitCard (String txt, String spliter, int order){
        try{
            String[] array = txt.split(spliter);
            return array[order];
        }
        catch(Exception ex){
            return null;
        }
    }

    public static String getDate (){
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("MMM,dd yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

}
