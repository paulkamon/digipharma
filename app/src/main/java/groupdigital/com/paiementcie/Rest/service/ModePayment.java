package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;
import android.util.Log;

import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseModePayment;
import groupdigital.com.paiementcie.Rest.response.ReponseUserInfo;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by digital on 26/11/2017.
 */

public class ModePayment {
    private RetrofitBuilder retrofitBuilder;
    private ResponseListener responseListener;
    private String url;
    private String route;
    private Context mContext;

    public ModePayment(ResponseListener r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseListener =r;
        this.route = url;
        this.mContext = ctx;
    }

    public void startRequest(String url){
        this.url = url;
        responseListener.OnRequestStart();
        retrofitRequest contentRequest = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseModePayment> callResponse = contentRequest.found(this.url);
        callResponse.enqueue(new Callback<ReponseModePayment>() {
            @Override
            public void onResponse(Call<ReponseModePayment> call, Response<ReponseModePayment> response) {
                try {
                    if(response.code()==200){
                        responseListener.OnRequestStop();
                        responseListener.getResponse(response.body());
                    }
                    else if(response.code()==400){
                        responseListener.getError();
                    }
                }
                catch (Exception e){
                    responseListener.getError();
                }
            }

            @Override
            public void onFailure(Call<ReponseModePayment> call, Throwable t) {
                responseListener.OnRequestStop();
                responseListener.getError();
            }
        });
    }
}
