package groupdigital.com.paiementcie.Rest.response;

import groupdigital.com.paiementcie.Content.Dto.OperateurDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 27/11/2017.
 */

public class ReponseOperator implements Serializable {
    private boolean status;
    private String message;
    private List<OperateurDto> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OperateurDto> getData() {
        return data;
    }

    public void setData(List<OperateurDto> data) {
        this.data = data;
    }
}
