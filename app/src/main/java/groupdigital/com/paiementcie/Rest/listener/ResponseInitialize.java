package groupdigital.com.paiementcie.Rest.listener;

/**
 * Created by digital on 09/05/2017.
 */

public interface ResponseInitialize<T> {
    public void getResponseInitia(T retour);
    public void  OntStartInitia();
    public void  OnStopInitia();
    public void getErrorInitia();
}
