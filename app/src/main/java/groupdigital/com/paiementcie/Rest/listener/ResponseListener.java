package groupdigital.com.paiementcie.Rest.listener;

/**
 * Created by digital on 09/05/2017.
 */

public interface ResponseListener<T> {
    public void getResponse(T response);
    public void  OnRequestStart();
    public void  OnRequestStop();
    public void getError();
}
