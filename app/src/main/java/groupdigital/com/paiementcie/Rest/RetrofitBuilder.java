package groupdigital.com.paiementcie.Rest;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import groupdigital.com.paiementcie.Activities.splash.SplashActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitBuilder {

    private static final String BASE_URL = Route.BASE;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Connection", "close");
            request = builder.build();
            return chain.proceed(request);
        }
    });
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());


    public RetrofitBuilder() {
    }

    public static <S> S createService(
            Class<S> serviceClass, final String authToken) {
        //httpClient.addInterceptor(new LoggingInterceptor());

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        httpClient.addNetworkInterceptor(new StethoInterceptor());


        httpClient.connectTimeout(Constants.TIME_NETWORK_RESPONSE, TimeUnit.SECONDS);
        httpClient.readTimeout(Constants.TIME_NETWORK_RESPONSE, TimeUnit.SECONDS);
        builder.client(httpClient.build());
        return builder.build().create(serviceClass);
    }




}
