package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;

import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.BankCardDto;
import groupdigital.com.paiementcie.Content.Dto.BankPaymentDto;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by digital on 27/11/2017.
 */

public class BankTask {
    private RetrofitBuilder retrofitBuilder;
    private ResponseListener responseListener;
    private String url;
    private String route;
    private Context mContext;

    public BankTask (ResponseListener r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseListener =r;
        this.route = url;
        this.mContext = ctx;
    }

    public void FinalisePayment (String url, BankPaymentDto models){
        this.url = url;
        retrofitRequest request = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponsePay> callResponse = request.payByBankPayement(this.url, models);
        callResponse.enqueue(new Callback<ReponsePay>() {
            @Override
            public void onResponse(Call<ReponsePay> call, Response<ReponsePay> reponse) {
                try {
                    if(reponse.code()==200){
                        responseListener.getResponse(reponse.body());
                    }
                    else{
                        responseListener.getError();
                    }
                }
                catch (Exception e){
                    responseListener.getError();
                }
            }
            @Override
            public void onFailure(Call<ReponsePay> call, Throwable t) {
                responseListener.getError();
            }
        });
    }

    public ResponseListener getResponseListener() {
        return responseListener;
    }

    public void setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
    }
}
