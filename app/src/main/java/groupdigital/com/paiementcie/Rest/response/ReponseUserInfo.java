package groupdigital.com.paiementcie.Rest.response;

import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class ReponseUserInfo implements Serializable{
    private boolean status;
    private String message;
    private UserInformationDto data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserInformationDto getData() {
        return data;
    }

    public void setData(UserInformationDto data) {
        this.data = data;
    }
}
