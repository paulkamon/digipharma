package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;
import android.util.Log;

import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseBilan;
import groupdigital.com.paiementcie.Rest.response.ReponseCollecteur;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by digital on 26/11/2017.
 */

public class BilanService {
    private RetrofitBuilder retrofitBuilder;
    private ResponseListener responseListener;
    private String url;
    private String route;
    private Context mContext;

    public BilanService(ResponseListener r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseListener =r;
        this.route = url;
        this.mContext = ctx;
    }

    public void findAll(String url){
        this.url = url;
        responseListener.OnRequestStart();
        retrofitRequest contentRequest = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseBilan> callResponse = contentRequest.getBilan(this.url);
        callResponse.enqueue(new Callback<ReponseBilan>() {
            @Override
            public void onResponse(Call<ReponseBilan> call, Response<ReponseBilan> response) {
                try {
                    if(response.code()==200){
                        responseListener.OnRequestStop();
                        responseListener.getResponse(response.body());
                    }
                    else if(response.code()==400){
                        responseListener.getError();
                    }
                    else if(response.code() > 400){
                        responseListener.getError();
                    }
                }
                catch (Exception e){
                    responseListener.getError();
                }
            }

            @Override
            public void onFailure(Call<ReponseBilan> call, Throwable t) {
                responseListener.OnRequestStop();
                responseListener.getError();
            }
        });
    }
}
