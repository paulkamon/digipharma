package groupdigital.com.paiementcie.Rest.response;

import groupdigital.com.paiementcie.Content.Dto.Collecteur;

import java.io.Serializable;

/**
 * Created by digital on 25/11/2017.
 */

public class ReponseCollecteur implements Serializable {
    private Collecteur data ;
    private boolean status;
    private String message;

    public Collecteur getData() {
        return data;
    }

    public void setData(Collecteur data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
