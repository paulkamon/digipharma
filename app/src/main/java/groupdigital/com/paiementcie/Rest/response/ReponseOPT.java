package groupdigital.com.paiementcie.Rest.response;

import groupdigital.com.paiementcie.Content.Dto.OtpDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 27/11/2017.
 */

public class
ReponseOPT implements Serializable {
    private boolean status;
    private String message;
    private OtpDto data;
    private String id_transaction;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OtpDto getData() {
        return data;
    }

    public void setData(OtpDto data) {
        this.data = data;
    }

    public String getId_transaction() {
        return id_transaction;
    }

    public void setId_transaction(String id_transaction) {
        this.id_transaction = id_transaction;
    }
}
