package groupdigital.com.paiementcie.Rest.response;

import groupdigital.com.paiementcie.Content.Dto.BilanJournalierDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class ReponseBilan implements Serializable {
    private boolean status;
    private String message;
    private List<BilanJournalierDto> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BilanJournalierDto> getData() {
        return data;
    }

    public void setData(List<BilanJournalierDto> data) {
        this.data = data;
    }
}
