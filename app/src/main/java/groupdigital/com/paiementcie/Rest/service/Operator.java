package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;

import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.listener.Responses;
import groupdigital.com.paiementcie.Rest.response.ReponseOperator;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by digital on 27/11/2017.
 */

public class Operator {
    private RetrofitBuilder retrofitBuilder;
    private Responses responseListener;
    private String url;
    private String route;
    private Context mContext;

    public Operator(Responses r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseListener =r;
        this.route = url;
        this.mContext = ctx;
    }

    public void listOperator(String url){
        this.url = url;
        responseListener.OnRequestStartList();
        retrofitRequest contentRequest = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseOperator> callResponse = contentRequest.listGet(this.url);
        callResponse.enqueue(new Callback<ReponseOperator>() {
            @Override
            public void onResponse(Call<ReponseOperator> call, Response<ReponseOperator> response) {
                try {
                    if(response.code()==200){
                        responseListener.OnRequestStopList();
                        responseListener.getResponseList(response.body());
                    }
                    else if(response.code()==400){
                        responseListener.onError();
                    }
                }
                catch (Exception e){
                    responseListener.onError();
                }
            }

            @Override
            public void onFailure(Call<ReponseOperator> call, Throwable t) {
                responseListener.OnRequestStopList();
                responseListener.onError();
            }
        });
    }
}
