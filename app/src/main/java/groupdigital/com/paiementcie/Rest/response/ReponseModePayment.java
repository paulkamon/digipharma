package groupdigital.com.paiementcie.Rest.response;

import groupdigital.com.paiementcie.Content.Dto.ModePaymentDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digital on 26/11/2017.
 */

public class ReponseModePayment implements Serializable {
    private boolean status;
    private String message;
    private List<ModePaymentDto> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ModePaymentDto> getData() {
        return data;
    }

    public void setData(List<ModePaymentDto> data) {
        this.data = data;
    }
}
