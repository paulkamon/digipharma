package groupdigital.com.paiementcie.Rest.retrofitInterface;

import org.json.JSONObject;

import java.util.Map;

import groupdigital.com.paiementcie.Content.Dto.BankCardDto;
import groupdigital.com.paiementcie.Content.Dto.BankPaymentDto;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.InitialisePaymentMobileDto;
import groupdigital.com.paiementcie.Content.Dto.MobilePaymentDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Rest.listener.ResponseInitialize;
import groupdigital.com.paiementcie.Rest.response.ReponseBilan;
import groupdigital.com.paiementcie.Rest.response.ReponseCollecteur;
import groupdigital.com.paiementcie.Rest.response.ReponseModePayment;
import groupdigital.com.paiementcie.Rest.response.ReponseOPT;
import groupdigital.com.paiementcie.Rest.response.ReponseOperator;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.response.ReponseUserInfo;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by digital on 09/05/2017.
 */

public interface retrofitRequest {
    @GET()
    Call<ReponseBilan> getBilan(@Url String url);

    @GET()
    Call<ReponseUserInfo> userInformation(@Url String url);

    @GET()
    Call<ReponseModePayment> found(@Url String url);

    @POST()
    Call<ReponseCollecteur> postData(@Url String url, @Body Collecteur models);

    @POST()
    Call<ReponsePay> payInvoice(@Url String url, @Body PayFactureDto models);

    @POST()
    Call<ReponsePay> payByMomo(@Url String url, @Body MobilePaymentDto models);

    @GET()
    Call<ReponseOperator> listGet(@Url String url);

    @POST()
    Call<ReponseOPT> initialseStep(@Url String url, @Body InitialisePaymentMobileDto models);

    @POST()
    Call<ReponseOPT> initializeCardBank(@Url String url, @Body BankCardDto models);

    @POST()
    Call<ReponsePay> payByCardBank(@Url String url, @Body BankCardDto models);

    @POST()
    Call<ReponseOPT> initialseStepBank(@Url String url, @Body BankPaymentDto models);

    @POST()
    Call<ReponsePay> payByBankPayement(@Url String url, @Body BankPaymentDto models);

    @POST("http://178.33.227.9:2050/v0.1/paiements/carte_bancaire")
    Call<ReponsePay> cardPay(@Body String data);

}
