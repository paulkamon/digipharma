package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;
import android.util.Log;

import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.BankCardDto;
import groupdigital.com.paiementcie.Content.Dto.BankPaymentDto;
import groupdigital.com.paiementcie.Content.Dto.InitialisePaymentMobileDto;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseInitialize;
import groupdigital.com.paiementcie.Rest.response.ReponseOPT;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by digital on 27/11/2017.
 */

public class InitializePaymentTask {
    private RetrofitBuilder retrofitBuilder;
    private ResponseInitialize responseInitialize;
    private String url;
    private String route;
    private Context mContext;

    public InitializePaymentTask (ResponseInitialize r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseInitialize =r;
        this.route = url;
        this.mContext = ctx;
    }

    public ResponseInitialize getResponseInitialize() {
        return responseInitialize;
    }

    public void setResponseInitialize(ResponseInitialize responseInitialize) {
        this.responseInitialize = responseInitialize;
    }


    public void initialiseProcess (String url, InitialisePaymentMobileDto models){
        this.url = url;
        retrofitRequest request = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseOPT> callResponse = request.initialseStep(this.url, models);
        callResponse.enqueue(new Callback<ReponseOPT>() {
            @Override
            public void onResponse(Call<ReponseOPT> call, Response<ReponseOPT> reponse) {
                try {
                    if(reponse.code()==200){
                        responseInitialize.getResponseInitia(reponse.body());
                    }
                    else{
                        responseInitialize.getErrorInitia();
                    }
                }
                catch (Exception e){
                    responseInitialize.getErrorInitia();
                }
            }
            @Override
            public void onFailure(Call<ReponseOPT> call, Throwable t) {
                responseInitialize.getErrorInitia();
            }
        });
    }


    public void initialiseProcessBank (String url, BankPaymentDto models){
        this.url = url;
        retrofitRequest request = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseOPT> callResponse = request.initialseStepBank(this.url, models);
        callResponse.enqueue(new Callback<ReponseOPT>() {
            @Override
            public void onResponse(Call<ReponseOPT> call, Response<ReponseOPT> reponse) {
                try {
                    if(reponse.code()==200){
                        responseInitialize.getResponseInitia(reponse.body());
                    }
                    else{
                        responseInitialize.getErrorInitia();
                    }
                }
                catch (Exception e){
                    responseInitialize.getErrorInitia();
                }
            }
            @Override
            public void onFailure(Call<ReponseOPT> call, Throwable t) {
                responseInitialize.getErrorInitia();
            }
        });
    }

    public void initialiseCardBank (String url, BankCardDto models){
        this.url = url;
        retrofitRequest request = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseOPT> callResponse = request.initializeCardBank(this.url, models);
        callResponse.enqueue(new Callback<ReponseOPT>() {
            @Override
            public void onResponse(Call<ReponseOPT> call, Response<ReponseOPT> reponse) {
                try {
                    if(reponse.code()==200){
                        responseInitialize.getResponseInitia(reponse.body());
                    }
                    else{
                        responseInitialize.getErrorInitia();
                    }
                }
                catch (Exception e){
                    responseInitialize.getErrorInitia();
                }
            }
            @Override
            public void onFailure(Call<ReponseOPT> call, Throwable t) {
                responseInitialize.getErrorInitia();
            }
        });
    }
}
