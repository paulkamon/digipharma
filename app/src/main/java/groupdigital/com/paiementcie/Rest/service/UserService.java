package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;
import android.util.Log;


import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseCollecteur;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DIGITAL on 31/08/2017.
 */

public class UserService {

    private RetrofitBuilder retrofitBuilder;
    private ResponseListener responseListener;
    private String url;
    private String route;
    private Context mContext;

    public UserService(ResponseListener r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseListener =r;
        this.route = url;
        this.mContext = ctx;
    }


    public ResponseListener getResponseListener() {
        return responseListener;
    }

    public void setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
    }

    public void connected (String url, Collecteur models){
        this.url = url;
        retrofitRequest request = retrofitBuilder.createService(retrofitRequest.class,"");
        Call<ReponseCollecteur> callResponse = request.postData(this.url, models);

        callResponse.enqueue(new Callback<ReponseCollecteur>() {
            @Override
            public void onResponse(Call<ReponseCollecteur> call, Response<ReponseCollecteur> reponse) {
                try {
                    if(reponse.code()==200){
                        responseListener.getResponse(reponse.body());
                    }
                    else{
                        responseListener.getError();
                    }
                }
                catch (Exception e){
                    responseListener.getError();
                }
            }

            @Override
            public void onFailure(Call<ReponseCollecteur> call, Throwable t) {
                Log.i("OK--T", t.getMessage());
                responseListener.getError();
            }
        });
    }


}
