package groupdigital.com.paiementcie.Rest.listener;

/**
 * Created by digital on 09/05/2017.
 */

public interface Responses<T> {
    public void getResponseList(T response);
    public void  OnRequestStartList();
    public void  OnRequestStopList();
    public void  onError();
}
