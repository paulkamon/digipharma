package groupdigital.com.paiementcie.Rest.response;

import java.util.List;

import groupdigital.com.paiementcie.Content.Dto.FactureDto;

public class CardPayement {
    private String canal;
    private String numero_carte;
    private String montant;

    public CardPayement() {
    }

    public CardPayement(String canal, String numero_carte, String montant) {
        this.canal = canal;
        this.numero_carte = numero_carte;
        this.montant = montant;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getNumero_carte() {
        return numero_carte;
    }

    public void setNumero_carte(String numero_carte) {
        this.numero_carte = numero_carte;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }
}
