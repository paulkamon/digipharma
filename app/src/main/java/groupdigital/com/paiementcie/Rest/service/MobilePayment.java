package groupdigital.com.paiementcie.Rest.service;

import android.content.Context;

import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseMobilePayment;
import groupdigital.com.paiementcie.Rest.response.ReponseOperator;
import groupdigital.com.paiementcie.Rest.retrofitInterface.retrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by digital on 27/11/2017.
 */

public class MobilePayment {
    private RetrofitBuilder retrofitBuilder;
    private ResponseListener responseListener;
    private String url;
    private String route;
    private Context mContext;

    public MobilePayment(ResponseListener r, String url, Context ctx) {
        this.retrofitBuilder = AutoLoad.getRetrofitBuilder();
        responseListener =r;
        this.route = url;
        this.mContext = ctx;
    }

}
