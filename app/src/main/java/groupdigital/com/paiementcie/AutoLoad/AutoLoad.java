package groupdigital.com.paiementcie.AutoLoad;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.facebook.stetho.Stetho;

import groupdigital.com.paiementcie.Rest.RetrofitBuilder;
import groupdigital.com.paiementcie.Utilitaire.AppSharedPreferences;


public class AutoLoad extends Application {
    public static final String TAG = Application.class.getSimpleName();
    private static AutoLoad mInstance;
    private static Context context;
    private static AppSharedPreferences prefs;
    public static String stringDeviceId;
    private static RetrofitBuilder retrofitBuilder;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        prefs = new AppSharedPreferences(context);
        //retrofitBuilder  = new RetrofitBuilder();
        Stetho.initializeWithDefaults(this);
    }



    /**
     *
     * @return
     */
    public static Context getContext() {
        return context;
    }

    /**
     *
     * @return
     */
    public static AppSharedPreferences getPrefs() {
        return prefs;
    }

    /**
     *
     * @param context
     * @param newTopActivityClass
     */
    public static void startNewActivity(Context context, Class<? extends Activity> newTopActivityClass) {
        Intent intent = new Intent(context, newTopActivityClass);
        context.startActivity(intent);
    }



    /**
     *
     * @param context
     * @param newTopActivityClass
     */
    public static void startNewTopActivity(Context context, Class<? extends Activity> newTopActivityClass) {
        Intent intent = new Intent(context, newTopActivityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
        }
        context.startActivity(intent);
    }


    /**
     *
     * @param context
     * @param intent
     */
    public static void startNewTopActivityFromIntent(Context context, Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
        }
        context.startActivity(intent);
    }

    public static synchronized AutoLoad getInstance() {
        return mInstance;
    }

    public static RetrofitBuilder getRetrofitBuilder() {
        return retrofitBuilder;
    }

    public static void setRetrofitBuilder(RetrofitBuilder retrofitBuilder) {
        AutoLoad.retrofitBuilder = retrofitBuilder;
    }




}
