package groupdigital.com.paiementcie.Activities.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import groupdigital.com.paiementcie.Activities.flow.UserInforamtionActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseUserInfo;
import groupdigital.com.paiementcie.Rest.service.SearchService;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.lang.reflect.Type;

public class SearchByNumberActivity extends AppCompatActivity implements ResponseListener<ReponseUserInfo> {
    private Typeface BoldCondensed, RobotoBold;
    private TextView headerTitle;
    private Button searchButton;
    private EditText idClient;
    private ProgressDialog nDialog;
    private Intent intent;
    private String sCustomerId;
    private SearchService searchService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_number);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.recherh) + "...");
        nDialog.setCancelable(true);
        nDialog.setCanceledOnTouchOutside(true);
        nDialog.setIndeterminate(false);

        ImageView bClose = (ImageView) findViewById(R.id.fermer_cmpte);
        bClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final Activity activity = this;
        BoldCondensed = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        searchButton = (Button)findViewById(R.id._sign_search);
        headerTitle = (TextView)findViewById(R.id.titleClose);
        idClient = (EditText)findViewById(R.id.pseudo);

        searchButton.setTypeface(RobotoBold);
        headerTitle.setTypeface(BoldCondensed);
        idClient.setTypeface(RobotoBold);


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void search() {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValideField()){
                //
                LaucheRest(sCustomerId);
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }


    private boolean isValideField() {
        String vCustomerId = idClient.getText().toString().trim();
        if(vCustomerId.isEmpty() || vCustomerId == ""){
            nDialog.dismiss();
            requestFocus(idClient);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            sCustomerId = vCustomerId.toLowerCase();
            return true;
        }
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void LaucheRest(String customerNumer) {
        idClient.setText("");
        if (Connectivity.isConnected(this)) {
            nDialog.show();
            String mParam = Route.FIND_CONTRIBUABLE+"?id="+customerNumer;
            searchService = new SearchService(this, mParam, this);
            searchService.launchRequest(mParam);
        }
    }

    @Override
    public void getResponse(ReponseUserInfo response) {
        nDialog.dismiss();
        try{
            if(response.isStatus()){
                //Log.i("OK-FOND", response.getData().);
                intent = new Intent(this, UserInforamtionActivity.class);
                intent.putExtra("data", response.getData());
                startActivity(intent);
                //finish();
            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", getResources().getString(R.string.noFound));
                intent.putExtra("key", "close");
                startActivity(intent);
            }
        }
        catch (Exception e){
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.impossible));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        //*if ((pDialog != null) && pDialog.isShowing())
        //pDialog.show();
        nDialog = null;
    }
}
