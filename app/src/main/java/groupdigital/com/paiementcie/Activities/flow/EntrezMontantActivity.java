package groupdigital.com.paiementcie.Activities.flow;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;


public class EntrezMontantActivity extends AppCompatActivity {
    private Button btnPay;
    private EditText montant;
    private Intent intent;
    private UserInformationDto userInformationDto;
    private Typeface Material, RobotoBold, RobotoLight;
    private int totalSomme = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrez_montant);

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
        RobotoLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        btnPay = (Button)findViewById(R.id._sign_validate);
        montant = (EditText) findViewById(R.id.montant);

        btnPay.setTypeface(RobotoBold);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.i("OK--GHDD", String.valueOf(totalSomme));
                if (!montant.getText().toString().equals("")) {
                    totalSomme = Integer.valueOf(montant.getText().toString());
                    if (totalSomme == 0) {
                        intent = new Intent(EntrezMontantActivity.this, AlertActivity.class);
                        intent.putExtra("msg", getResources().getString(R.string.entrez_le_montant));
                        intent.putExtra("key", "close");
                        startActivity(intent);
                    } else {
                        intent = new Intent(EntrezMontantActivity.this, ModePayementActivity.class);
                        intent.putExtra("montantFacture", totalSomme);
                        intent.putExtra("infoUserDto", userInformationDto);
                        startActivity(intent);
                        //finish();
                    }
                }else {
                    intent = new Intent(EntrezMontantActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.entrez_le_montant));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                }
            }
        });

    }


}
