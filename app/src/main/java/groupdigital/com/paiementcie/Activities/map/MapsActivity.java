package groupdigital.com.paiementcie.Activities.map;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Content.Dto.AddressDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private Intent intent;
    private double latitude, longitude;
    private Marker mCurrLocationMarker;
    private TextView info_position_sur_carte;


    private CameraPosition cameraPosition;
    private static final int DEFAULT_ZOOM = 11;
    private MarkerOptions markerOptions;
    private Type type;
    private Geocoder geocoder;
    private List<Address> addresses;
    private LatLng latLng;
    private Gson gson;
    private String markOptions, taxPayerName;
    private AddressDto addressDto;
    private RelativeLayout actionUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maps);

        intent = getIntent();
        latitude  = intent.getDoubleExtra("getLat", 0);
        longitude  = intent.getDoubleExtra("getLong", 0);
        taxPayerName = intent.getStringExtra("taxpayerName");
        Typeface RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        actionUser = (RelativeLayout)findViewById(R.id.actionUser);
        info_position_sur_carte = (TextView)findViewById(R.id.info_position_sur_carte);
        TextView titreHeader = (TextView)findViewById(R.id.titreHeader);
        titreHeader.setTypeface(RobotoBold);

        ImageView keyboard = (ImageView)findViewById(R.id.keyboardBack);
        keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addressDto = new AddressDto();
        markerOptions = new MarkerOptions();
        geocoder = new Geocoder(this, Locale.getDefault());


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        initialiseMap();
    }

    private void initialiseMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng( latitude, longitude),5.0f) );
        latLng = new LatLng(latitude, longitude);

        try {
            //Vérifie connection
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            //Log.i("OK----ADRESSE", addresses.toString());
            if (Connectivity.isConnected(MapsActivity.this)) {
                if (null != addresses && addresses.size() > 0) {
                    String lines = addresses.get(0).getAddressLine(0);
                    String subLocality = addresses.get(0).getSubLocality();
                    String region = addresses.get(0).getSubAdminArea();
                    markOptions = taxPayerName+" | "+region;

                    /*if(subLocality != null){
                        markOptions = taxPayerName+" | "+region;
                        //markOptions = taxPayerName+" | "+region+" | "+subLocality + " - " + lines;
                    }
                    else{
                        markOptions = taxPayerName+" | "+region+" - "+lines;
                        //markOptions = taxPayerName+" | "+region+" - "+lines;
                    }*/
                    String viex = "";
                    if(subLocality != null){
                        viex = region + " - " + subLocality + " - "+lines ;
                    }
                    else{
                        viex = region + " - "+lines;
                    }

                    actionUser.setVisibility(View.VISIBLE);
                    info_position_sur_carte.setText(viex);
                }
                else {
                    intent = new Intent(MapsActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.no_network));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                    markOptions = getResources().getString(R.string.impCord);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        addressDto.setLatitude(latitude);
        addressDto.setLongitude(longitude);
        addressDto.setAddressLine(markOptions);


        cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(DEFAULT_ZOOM)
                .build();

        markerOptions.position(latLng);
        markerOptions.title(markOptions);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM));
    }

}