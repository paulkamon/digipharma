package groupdigital.com.paiementcie.Activities.flow;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import groupdigital.com.paiementcie.Activities.result.CarteBancaireActivity;
import groupdigital.com.paiementcie.Activities.result.CashActivity;
import groupdigital.com.paiementcie.Activities.result.ChequeActivity;
import groupdigital.com.paiementcie.Activities.result.CompteBancaireActivity;
import groupdigital.com.paiementcie.Activities.result.MobilePaymentActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Adapter.AdapterModePayment;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.ModePaymentDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.listener.ViewClickListener;
import groupdigital.com.paiementcie.Rest.response.ReponseModePayment;
import groupdigital.com.paiementcie.Rest.service.BilanService;
import groupdigital.com.paiementcie.Rest.service.ModePayment;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;
import groupdigital.com.paiementcie.dspread.DSPREADMain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModePayementActivity extends AppCompatActivity implements ResponseListener<ReponseModePayment>, ViewClickListener {
    private Intent intent;
    private TextView payment, titreHeader;
    private int montantFacture, idContribuable;
    private ProgressDialog nDialog;
    private Typeface Material, RobotoBold;
    private RecyclerView recyclerModePAY;
    private RelativeLayout dataNoExistModePAY, networkModePAY, dataExistModePAY;
    private FastScroller fastModePAY;
    private AdapterModePayment adapterModePayment;
    private List<ModePaymentDto> modePaymentDtoList = new ArrayList<ModePaymentDto>();
    private ModePayment modePaymentSve;
    private List<FactureDto> listChoixFacture;
    private UserInformationDto userDto;
    private boolean paulSmartCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mode_payement);

        //recupération de la pref de carte
        paulSmartCard = AutoLoad.getPrefs().getBoolean(Constants.PREF_SMARTCART, false);
        //Log.i("OK--paulSmartCard", String.valueOf(paulSmartCard));

        dataNoExistModePAY = (RelativeLayout) findViewById(R.id.dataNoExistModePAY);
        networkModePAY = (RelativeLayout) findViewById(R.id.networkModePAY);
        dataExistModePAY = (RelativeLayout) findViewById(R.id.dataExistModePAY);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.loading) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        intent = getIntent();
        montantFacture  = intent.getIntExtra("montantFacture", 0);
        listChoixFacture  = (List<FactureDto>) intent.getSerializableExtra("choice");
        userDto  = (UserInformationDto) intent.getSerializableExtra("infoUserDto");


        TextView devise = (TextView)findViewById(R.id.devise);
        TextView gauch = (TextView)findViewById(R.id.gauch);
        payment = (TextView)findViewById(R.id.payment);
        titreHeader = (TextView)findViewById(R.id.titreHeader);
        payment.setText(""+montantFacture+"");
        devise.setTypeface(RobotoBold);
        gauch.setTypeface(RobotoBold);
        payment.setTypeface(RobotoBold);
        titreHeader.setTypeface(Material);

        LaunchRest();

        ImageView keyboard = (ImageView)findViewById(R.id.fermer_cmpte);
        keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutoLoad.startNewActivity(ModePayementActivity.this, MobilePaymentActivity.class);
                finish();
            }
        });


        initializeView();
    }

    private void LaunchRest() {
        if (Connectivity.isConnected(this)) {
            nDialog.show();
            String mParam = Route.PAYMENT_MODE;
            modePaymentSve = new ModePayment(this, mParam, this);
            modePaymentSve.startRequest(mParam);
        }
        else{
            networkModePAY.setVisibility(View.VISIBLE);
        }
    }

    private void initializeView() { //,
        recyclerModePAY = (RecyclerView) findViewById(R.id.listPaymentMode);
        fastModePAY = (FastScroller) findViewById(R.id.fasterPaymentMode);
        recyclerModePAY.setHasFixedSize(true);
        recyclerModePAY.setLayoutManager(new LinearLayoutManager(ModePayementActivity.this));
        adapterModePayment = new AdapterModePayment(this, modePaymentDtoList, this);
        recyclerModePAY.setAdapter(adapterModePayment);
        fastModePAY.setRecyclerView(recyclerModePAY);
    }


    @Override
    public void getResponse(ReponseModePayment response) {
        try{
            if(response != null){
                nDialog.dismiss();
                if(response.getData().size() == 0){
                    dataNoExistModePAY .setVisibility(View.VISIBLE);
                }
                else if(response.getData().size() > 0){
                    dataExistModePAY .setVisibility(View.VISIBLE);
                    if(response!= null && response.isStatus()){
                        int sizeData = response.getData().size();
                        if(sizeData > 0){
                            dataExistModePAY .setVisibility(View.VISIBLE);
                            for(int i=0;i<sizeData;i++){
                                if (!paulSmartCard){
                                    int id = response.getData().get(i).getId();
                                    if(id != 1 && id !=2 && id !=5){
                                        modePaymentDtoList.add(
                                            new ModePaymentDto(
                                                response.getData().get(i).getId(),
                                                response.getData().get(i).getModePaiement()
                                            )
                                        );
                                    }
                                }
                                else{
                                    modePaymentDtoList.add(
                                        new ModePaymentDto(
                                            response.getData().get(i).getId(),
                                            response.getData().get(i).getModePaiement()
                                        )
                                    );
                                }
                            }
                            adapterModePayment.notifyDataSetChanged();
                            OnRequestStop();
                        }
                        else if(sizeData == 0){
                            dataNoExistModePAY .setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
        catch (Exception e){
            nDialog.dismiss();
            networkModePAY.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        networkModePAY .setVisibility(View.VISIBLE);
    }

    @Override
    public void onClicItemWithParam(int id, String action) {
        if(action.equals("token")){
            int pageId = modePaymentDtoList.get(id).getId();
            if(pageId == 2){
                intent = new Intent(ModePayementActivity.this, ChequeActivity.class);
                intent.putExtra("montantFacture", montantFacture);
            }
            else if(pageId == 1){
                intent = new Intent(ModePayementActivity.this, CashActivity.class);
                intent.putExtra("montantFacture", montantFacture);
            }
            else if(pageId == 3){
                intent = new Intent(ModePayementActivity.this, MobilePaymentActivity.class);
                intent.putExtra("montantFacture", montantFacture);

            }
            else if(pageId == 4){
               // intent = new Intent(ModePayementActivity.this, CarteBancaireActivity.class);
                intent = new Intent(ModePayementActivity.this, DSPREADMain.class);
                intent.putExtra("montantFacture", montantFacture);
                //intent.putExtra("numeroFacture", listChoixFacture);
                //intent.putExtra("nomClient", userDto);
                // intent = new Intent(ModePayementActivity.this, DSPREADMain.class);
                intent.putExtra("connect_type", 3);
            }
            else if(pageId == 5){
                intent = new Intent(ModePayementActivity.this, CompteBancaireActivity.class);
                intent.putExtra("montantFacture", montantFacture);
                startActivity(intent);
            }
            //intent.putExtra("userDto", userDto);
            //intent.putExtra("listChoixFacture", (Serializable) listChoixFacture);
            startActivity(intent);
            //finish();
        }
    }
}
