package groupdigital.com.paiementcie.Activities.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import groupdigital.com.paiementcie.Alert.DeconnecterActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.R;

public class TaxCollectActivity extends AppCompatActivity {
    private TextView headerTitle, txtCollect, txtBilan;
    public static final String MESSAGE_PROGRESS = "OK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tax_collect);

        Typeface RobotoCondensed = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Condensed.ttf");
        Typeface BoldCondensed = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");

        txtBilan = (TextView)findViewById(R.id.txtB);
        txtCollect = (TextView)findViewById(R.id.collecte);
        headerTitle = (TextView)findViewById(R.id.titleEmail);
        LinearLayout BtnCollect = (LinearLayout)findViewById(R.id.btn_collect);
        LinearLayout BtnBilan = (LinearLayout)findViewById(R.id.btn_bilan_journalier);
        LinearLayout bSetting = (LinearLayout) findViewById(R.id.click_setting);
        LinearLayout bAgent = (LinearLayout) findViewById(R.id.click_agent);
        TextView txtD = (TextView) findViewById(R.id.txtD);
        TextView txtE = (TextView) findViewById(R.id.txtE);
        ImageView quit = (ImageView)findViewById(R.id.imageView2);


        txtBilan.setTypeface(RobotoCondensed);
        txtCollect.setTypeface(RobotoCondensed);
        headerTitle.setTypeface(BoldCondensed);
        txtD.setTypeface(RobotoCondensed);
        txtE.setTypeface(RobotoCondensed);

        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TaxCollectActivity.this, DeconnecterActivity.class);
                intent.putExtra("key", 1);
                intent.putExtra("msg", getResources().getString(R.string.logout));
                startActivity(intent);
            }
        });

        BtnBilan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutoLoad.startNewActivity(TaxCollectActivity.this, BilanJournalierActivity.class);
            }
        });

        BtnCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutoLoad.startNewActivity(TaxCollectActivity.this, CollectActivity.class);
            }
        });
    }

    @Override
    public void onBackPressed() {
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }*/
        //super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(MESSAGE_PROGRESS)){
                final String typeMsg = intent.getStringExtra("typeMsg");
                if(typeMsg.equals("logout")){
                    LocalBroadcastManager.getInstance(TaxCollectActivity.this).unregisterReceiver(broadcastReceiver);
                    //finish();
                    //super.onBackPressed();
                }
            }
        }
    };

    private void registerReceiver(){
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        int id= android.os.Process.myPid();
        android.os.Process.killProcess(id);
        super.onDestroy();
        finish();
    }
}
