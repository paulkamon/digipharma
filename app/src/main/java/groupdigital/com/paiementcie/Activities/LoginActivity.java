package groupdigital.com.paiementcie.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import groupdigital.com.paiementcie.Activities.flow.EntrezMontantActivity;
import groupdigital.com.paiementcie.Activities.home.TaxCollectActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseCollecteur;
import groupdigital.com.paiementcie.Rest.service.UserService;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

public class LoginActivity extends AppCompatActivity implements ResponseListener<ReponseCollecteur> {

    private EditText vPseudo, vPassword;
    private Typeface RobotoBold, RobotoMedium;
    private Button goTo;
    private EditText pseudo, pwdEditText;
    private ProgressDialog nDialog;
    private Intent intent;
    private String prefUser = Constants.PREF_NAME;
    private Gson gson;
    private Collecteur collecteur;
    private String sPwd, sPseudo, dataPrefUser, JsonPrefUser;
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.wait) + "...");
        nDialog.setCancelable(true);
        nDialog.setCanceledOnTouchOutside(true);
        nDialog.setIndeterminate(false);

        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        RobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");

        goTo = (Button) findViewById(R.id._sign_in_button);
        vPseudo = (EditText) findViewById(R.id.pseudo);
        vPassword = (EditText) findViewById(R.id.password);

        vPseudo.setTypeface(RobotoMedium);
        vPassword.setTypeface(RobotoMedium);
        goTo.setTypeface(RobotoBold);


        pseudo = (EditText) findViewById(R.id.pseudo);
        pwdEditText = (EditText) findViewById(R.id.password);

        goTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                postConnetedUser();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }


    private void postConnetedUser() {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValidePhone() && isValidePwd()){
                handlerPost(sPwd, sPseudo);
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void handlerPost(String sPwd, String sPseudo) {
        collecteur = new Collecteur();
        collecteur.setUsername(sPseudo);
        collecteur.setPassword(sPwd);

        userService = new UserService(this, Route.CONECTED_ACCOUNT, null);
        userService.setResponseListener(this);
        userService.connected(Route.CONECTED_ACCOUNT, collecteur);
    }

    private boolean isValidePhone()
    {
        String vLastNam = pseudo.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(pseudo);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            sPseudo = vLastNam;
            return true;
        }
    }

    private boolean isValidePwd()
    {
        String vLastNam = pwdEditText.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam.equals("")){
            nDialog.dismiss();
            requestFocus(pwdEditText);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            sPwd = vLastNam;
            return true;
        }
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void getResponse(ReponseCollecteur response) {
        nDialog.dismiss();
        if(response.isStatus()){
            collecteur = new Collecteur();
            dataPrefUser = AutoLoad.getPrefs().getString(this.prefUser, null);
            if(dataPrefUser == null){
                collecteur.setUsername(sPseudo);
                gson = new Gson();
                JsonPrefUser = gson.toJson(collecteur);

                AutoLoad.getPrefs().putString(this.prefUser, JsonPrefUser);
                pseudo.setText("");
                pwdEditText.setText("");

                AutoLoad.startNewActivity(LoginActivity.this, EntrezMontantActivity.class);
                finish();
            }
            else{
                AutoLoad.getPrefs().remove(this.prefUser);
                if(dataPrefUser == null){
                    collecteur.setUsername(sPseudo);
                    gson = new Gson();
                    JsonPrefUser = gson.toJson(collecteur);

                    AutoLoad.getPrefs().putString(this.prefUser, JsonPrefUser);
                    pseudo.setText("");
                    pwdEditText.setText("");

                    AutoLoad.startNewActivity(LoginActivity.this, EntrezMontantActivity.class);
                    finish();
                }
            }
        }
        else{
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", response.getMessage());
            intent.putExtra("key", "close");
            startActivity(intent);
            requestFocus(pseudo);
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
        requestFocus(pseudo);
    }
}
