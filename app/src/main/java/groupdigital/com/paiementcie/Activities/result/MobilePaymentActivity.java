package groupdigital.com.paiementcie.Activities.result;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import groupdigital.com.paiementcie.Activities.flow.EntrezMontantActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Alert.CallbackMobileMoneyActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.InitialisePaymentMobileDto;
import groupdigital.com.paiementcie.Content.Dto.MobilePaymentDto;
import groupdigital.com.paiementcie.Content.Dto.OperateurDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseInitialize;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.listener.Responses;
import groupdigital.com.paiementcie.Rest.response.ReponseOPT;
import groupdigital.com.paiementcie.Rest.response.ReponseOperator;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.service.InitializePaymentTask;
import groupdigital.com.paiementcie.Rest.service.Operator;
import groupdigital.com.paiementcie.Rest.service.PayRest;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

public class MobilePaymentActivity extends AppCompatActivity implements
        Responses<ReponseOperator>, ResponseInitialize<ReponseOPT>,
        ResponseListener<ReponsePay>
{
    private EditText codePassword, numero, montant;
    private Spinner type_payment, typeOperator;
    private Button payWithMobileAccount, btnInitializePayment;
    private ProgressDialog nDialog;
    private Typeface Material, RobotoBold;
    private Intent intent;
    private TextView titreHeader;
    private int montantFacture, idTypeOperator;
    private List<FactureDto> listFacturePay;
    private UserInformationDto userDto;
    private String prefUser = Constants.PREF_NAME;
    private Collecteur collecteur;
    private String user, sPhoneMobileAccount, codeOpt, vCodeOPT;
    private Type type;
    private Gson gson;

    private InitialisePaymentMobileDto initialisePaymentMobileDto;
    private OperateurDto operateurDto;
    private Operator operator;
    private List<OperateurDto> listeTypeOperateurDtos = new ArrayList<OperateurDto>();
    private List<String> list = new ArrayList<String>();
    private ArrayAdapter<String> dataAdapter;

    /*Initialise payment*/
    private InitializePaymentTask iTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mobile_payment);

        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        if(user != null){
            collecteur = gson.fromJson(user, type);
        }

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.patient) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        intent = getIntent();
        montantFacture  = intent.getIntExtra("montantFacture", 0);
        listFacturePay  = (List<FactureDto>) intent.getSerializableExtra("listChoixFacture");
        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        configuration();
    }

    private void configuration() {
        montant = (EditText) findViewById(R.id.montant);
        codePassword = (EditText) findViewById(R.id.codePassword);
        numero = (EditText) findViewById(R.id.numero);
        payWithMobileAccount = (Button)findViewById(R.id._sign_validate);
        btnInitializePayment = (Button)findViewById(R.id.initializePayment);
        titreHeader = (TextView)findViewById(R.id.titreHeader);

        titreHeader.setTypeface(Material);
        codePassword.setTypeface(RobotoBold);
        numero.setTypeface(RobotoBold);
        montant.setTypeface(RobotoBold);
        btnInitializePayment.setTypeface(RobotoBold);
        payWithMobileAccount.setTypeface(RobotoBold);


        montant.setText("Amount to be paid : "+montantFacture+" FCFA");
        //numero.setText(""+userDto.getContact()+"");

        type_payment = (Spinner) findViewById(R.id.type_payment_spinner);
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        type_payment.setAdapter(dataAdapter);
        type_payment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                }
                else if(position > 0){
                    String IdChoix  = String.valueOf(listeTypeOperateurDtos.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(collecteur != null){
            nDialog.show();
            takeListeOperator();
        }

        btnInitializePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startProcess();
            }
        });
        payWithMobileAccount.setVisibility(View.VISIBLE);
        payWithMobileAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //finishProcesse();
                startProcess();
            }
        });
        //btnInitializePayment.setVisibility(View.GONE);
        //payWithMobileAccount.setVisibility(View.VISIBLE);
    }

    private void finishProcesse() {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValidePhone() && isValideType()){

                handlerFinishProcesse(sPhoneMobileAccount,idTypeOperator);
            }
            else{
                nDialog.dismiss();
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", getResources().getString(R.string.bad_opt));
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(codePassword);
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void startProcess()
    {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValidePhone() && isValideType()){
                handlerProcess(sPhoneMobileAccount, idTypeOperator);
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }


    private void takeListeOperator() {
        String mParam = Route.OPERATOR_LIST;
        operator = new Operator(this, mParam, this);
        operator.listOperator(mParam);
    }

    @Override
    public void getResponseList(ReponseOperator response)
    {
        if (response.isStatus()){
            nDialog.dismiss();
            int sizeData = response.getData().size();
            if(sizeData > 0){
                for(int i=0; i<sizeData; i++){
                    listeTypeOperateurDtos.add(new OperateurDto(response.getData().get(i).getId(), response.getData().get(i).getLibelle()));
                    list.add(response.getData().get(i).getLibelle());
                    dataAdapter.notifyDataSetChanged();
                }
            }
        }
        else{
            finish();
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.impossible_frm));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    @Override
    public void OnRequestStartList() {

    }

    @Override
    public void OnRequestStopList() {

    }

    @Override
    public void onError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible_frm));
        intent.putExtra("key", "close");
        startActivity(intent);
    }

    private boolean isValidePhone()
    {
        String vLastNam = numero.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(numero);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            sPhoneMobileAccount = vLastNam;
            return true;
        }
    }

    private boolean isValideType()
    {
        int vType = (int) listeTypeOperateurDtos.get(type_payment.getSelectedItemPosition()).getId();
        if(vType == 0){
            nDialog.dismiss();
            requestFocus(type_payment);
            return false;
        }
        else{
            idTypeOperator = vType;
            //typeOperator = listeTypeOperateurDtos.get(type_payment.getSelectedItemPosition()).getLibelle();
            return true;
        }
    }

    private boolean isValideCodeOPT()
    {
        String vLastNam = codePassword.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(codePassword);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vCodeOPT = vLastNam;
            return true;
        }
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void handlerProcess(String phone, int idOperator)
    {
        initialisePaymentMobileDto = new InitialisePaymentMobileDto();
        initialisePaymentMobileDto.setMsisdn("225"+phone);
        initialisePaymentMobileDto.setMontant(montantFacture);

        iTask = new InitializePaymentTask(this, Route.INITIALISE_PAYMENT, this);
        iTask.setResponseInitialize(this);
        iTask.initialiseProcess(Route.INITIALISE_PAYMENT, initialisePaymentMobileDto);
    }


    @Override
    public void getResponseInitia(ReponseOPT retour)
    {
        nDialog.dismiss();
        try {
            if(retour.isStatus()){
                /**intent = new Intent(this, AlertActivity.class);
                 intent.putExtra("msg", retour.getMessage());
                 intent.putExtra("key", "close");
                 startActivity(intent);
                 numero.setEnabled(false);
                 numero.setInputType(InputType.TYPE_NULL);
                 //finish();**/
                finish();
                AutoLoad.startNewActivity(MobilePaymentActivity.this, EntrezMontantActivity.class);
                intent = new Intent(this, CallbackMobileMoneyActivity.class);
                intent.putExtra("notrans",retour.getId_transaction());
                startActivity(intent);

            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", retour.getMessage());
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(numero);
            }
        } catch (Exception e){
            Log.i("OK--Exception", e.getMessage());
        }
    }

    @Override
    public void OntStartInitia() {

    }

    @Override
    public void OnStopInitia() {

    }


    @Override
    public void getErrorInitia()
    {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
        requestFocus(numero);
    }


    private void handlerFinishProcesse(String phone, int idOperator) {
        initialisePaymentMobileDto = new InitialisePaymentMobileDto();
        initialisePaymentMobileDto.setMsisdn(phone);
        initialisePaymentMobileDto.setMontant(montantFacture);
        MobilePaymentDto mobilePaymentDto = new MobilePaymentDto();
        //mobilePaymentDto.setOtp(codeOpt);
        mobilePaymentDto.setNumeroMobile(initialisePaymentMobileDto.getMsisdn());
        mobilePaymentDto.setAmount(initialisePaymentMobileDto.getMontant());
        mobilePaymentDto.setUsername(collecteur.getUsername());
        mobilePaymentDto.setIdContribuable(userDto.getId());
        mobilePaymentDto.setIdModePaiement(3);
        mobilePaymentDto.setFactureDto(listFacturePay);

        PayRest payRest = new PayRest(this, Route.FINALISE_PAYMENT_BAY_MOMO, null);
        payRest.setResponseListener(this);
        payRest.FinalisePayByMomo(Route.FINALISE_PAYMENT_BAY_MOMO, mobilePaymentDto);
    }

    @Override
    public void getResponse(ReponsePay response)
    {
        nDialog.dismiss();
        /***try {
         if(response != null && response.isStatus()){
         finish();
         intent = new Intent(this, CongratulationActivity.class);
         intent.putExtra("nRecu", response.getMessage());
         intent.putExtra("info", (ArrayList<FactureDto>) listFacturePay);
         intent.putExtra("userDto", userDto);
         intent.putExtra("typePay", "Momo");
         startActivity(intent);
         }
         else{
         intent = new Intent(this, AlertActivity.class);
         intent.putExtra("msg", response.getMessage());
         intent.putExtra("key", "close");
         startActivity(intent);
         requestFocus(numero);
         }
         }catch (Exception e){
         Log.i("Ok--Error", e.getMessage());
         }**/
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }
}

