package groupdigital.com.paiementcie.Activities.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import groupdigital.com.paiementcie.Activities.flow.UserInforamtionActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseUserInfo;
import groupdigital.com.paiementcie.Rest.service.SearchService;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Route;

public class CollectActivity extends AppCompatActivity implements View.OnClickListener, ResponseListener<ReponseUserInfo> {

    private Typeface Material, RobotoBold, RobotoLight;
    private TextView headerTitle, txTreadQRCode;
    private Button searchButton;
    private EditText idClient;
    private SearchService searchService;
    private ProgressDialog nDialog;
    private Intent intent;
    private String sCustomerId, customerNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_collect);

        Typeface RobotoBlack = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Black.ttf");
        Typeface BoldCondensed = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        Typeface RobotoLight = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Light.ttf");

        TextView titleClose = (TextView)findViewById(R.id.titleClose);
        TextView textMsg = (TextView)findViewById(R.id.textMsg);
        textMsg.setTypeface(RobotoLight);
        titleClose.setTypeface(BoldCondensed);

        ImageView bClose = (ImageView) findViewById(R.id.fermer_cmpte);
        bClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button bNumContribuable = (Button) findViewById(R.id.nContribuable);
        //Button bPrintDigital = (Button) findViewById(R.id.flingerPrint);
        Button bQrCode = (Button) findViewById(R.id.qrCode);
        bQrCode.setOnClickListener(this);

        bNumContribuable.setTypeface(RobotoBlack);
        //bPrintDigital.setTypeface(RobotoBlack);
        bQrCode.setTypeface(RobotoBlack);

        bNumContribuable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connectivity.isConnected(CollectActivity.this)) {
                    AutoLoad.startNewActivity(CollectActivity.this, SearchByNumberActivity.class);
                    finish();
                }
                else{
                    intent = new Intent(CollectActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.no_network));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                }
                //finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.qrCode){
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        //Log.i("OK--scan", scanningResult.toString());
        if (scanningResult != null) {
            LaucheRest(scanningResult.getContents().toLowerCase());
        }
        else{
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.scanningCancelled));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void LaucheRest(String customerNumer) {
        if (Connectivity.isConnected(this)) {
            nDialog = new ProgressDialog(CollectActivity.this);
            nDialog.setMessage(getResources().getString(R.string.recherh) + "...");
            nDialog.setCancelable(true);
            nDialog.setCanceledOnTouchOutside(true);
            nDialog.setIndeterminate(false);
            nDialog.show();
            String mParam = Route.FIND_CONTRIBUABLE+"?id="+customerNumer;
            searchService = new SearchService(this, mParam, this);
            searchService.launchRequest(mParam);
        }
    }


    @Override
    public void getResponse(ReponseUserInfo response) {
        nDialog.dismiss();
        try{
            if(response.isStatus()){
                //Log.i("OK-FOND", response.getData().);
                //finish();
                intent = new Intent(this, UserInforamtionActivity.class);
                intent.putExtra("data", response.getData());
                startActivity(intent);
                //finish();
            }
            else{
                finish();
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", getResources().getString(R.string.noFound));
                intent.putExtra("key", "close");
                startActivity(intent);
            }
        }
        catch (Exception e){
            finish();
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.impossible));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        finish();
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        if ((nDialog != null) && nDialog.isShowing())
        //pDialog.show();
        nDialog = null;
    }
}
