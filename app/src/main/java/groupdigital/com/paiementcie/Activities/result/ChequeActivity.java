package groupdigital.com.paiementcie.Activities.result;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Alert.CongratulationActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.service.PayRest;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.lang.reflect.Type;
import java.util.List;

public class ChequeActivity extends AppCompatActivity implements ResponseListener<ReponsePay> {

    private ProgressDialog nDialog;
    private Typeface Material, RobotoBold;
    private Intent intent;
    private TextView titreHeader;
    private int montantFacture;
    private EditText checknumber, montantCheck;
    private String prefUser = Constants.PREF_NAME;
    private Collecteur collecteur;
    private PayFactureDto payFactureDto;
    private String user, checkNumber;
    private Type type;
    private Gson gson;
    private Button payCheck;
    private List<FactureDto> listFacturePay;
    private UserInformationDto userDto;
    private ImageView returnKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cheque);

        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        if(user != null){
            collecteur = gson.fromJson(user, type);
        }

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.loading) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        intent = getIntent();
        montantFacture  = intent.getIntExtra("montantFacture", 0);
        listFacturePay  = (List<FactureDto>) intent.getSerializableExtra("listChoixFacture");
        userDto  = (UserInformationDto) intent.getSerializableExtra("userDto");

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        checknumber = (EditText)findViewById(R.id.checknumber);
        montantCheck = (EditText)findViewById(R.id.montantCheck);
        payCheck = (Button)findViewById(R.id._payCheck);

        titreHeader = (TextView)findViewById(R.id.titreHeader);
        titreHeader.setTypeface(Material);

        montantCheck.setTypeface(RobotoBold);
        checknumber.setTypeface(RobotoBold);
        payCheck.setTypeface(RobotoBold);

        montantCheck.setText("Amount to be paid : "+montantFacture+" GHS");

        returnKey = (ImageView)findViewById(R.id.keyboardCheck);
        returnKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        payCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(collecteur != null){
                    nDialog.show();
                    PayInvoiceCheck();
                }
                /*AutoLoad.startNewActivity(ChequeActivity.this, CashActivity.class);
                finish();*/
            }
        });
    }

    private void PayInvoiceCheck() {
        if (Connectivity.isConnected(this)) {
            if(isValidCheck()){
                handlerPayAccount();
            }
        }
        else{
            intent = new Intent(ChequeActivity.this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void handlerPayAccount() {
        payFactureDto = new PayFactureDto();
        payFactureDto.setUsername(collecteur.getUsername());
        payFactureDto.setIdContribuable(userDto.getId());
        payFactureDto.setIdModePaiement(2);
        payFactureDto.setFactureDto(listFacturePay);

        PayRest payRest = new PayRest(this, Route.PAYMENT_FACTURE_CASH_CHEQUE, null);
        payRest.setResponseListener(this);
        payRest.payment(Route.PAYMENT_FACTURE_CASH_CHEQUE, payFactureDto);
    }


    public boolean isValidCheck() {
        String vLastNam = checknumber.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(checknumber);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            checkNumber = vLastNam;
            return true;
        }
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void getResponse(ReponsePay response) {
        nDialog.dismiss();
        if(response != null && response.isStatus()){
            finish();
            intent = new Intent(this, CongratulationActivity.class);
            intent.putExtra("nRecu", response.getMessage());
            intent.putExtra("info", payFactureDto);
            intent.putExtra("userDto", userDto);
            intent.putExtra("typePay", "cheque");
            startActivity(intent);
        }
        else{
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", response.getMessage());
            intent.putExtra("key", "close");
            startActivity(intent);
            requestFocus(checknumber);
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }
}
