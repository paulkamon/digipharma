package groupdigital.com.paiementcie.Activities.home.Settings;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.R;

import java.lang.reflect.Type;

public class SettingsActivity extends AppCompatActivity {

    private Intent intent;
    //private Agent agent;
    private Typeface BoldCondensed, RobotoBold, RobotoMedium;
    private TextView txt_nom, txt_mails, inpt_phone, pwd_change, btn_carte;
    private Button btnDeconnect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        /*Gson gson = new Gson();
        Type type = new TypeToken<Agent>() {}.getType();
        String agentConnect = AutoLoad.getPrefs().getString(Constants.PREF_NAME, null);
        agent = gson.fromJson(agentConnect, type);

        if (agentConnect != null) {
            setContentView(R.layout.activity_settings);

            ImageView close = (ImageView) findViewById(R.id.fermer_cmpte);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            BoldCondensed = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-BoldCondensed.ttf");
            RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
            RobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

            btnDeconnect = (Button) findViewById(R.id._sign_in_deconnect);
            TextView text = (TextView) findViewById(R.id.titleEmail);
            TextView profil = (TextView) findViewById(R.id.profil);
            TextView paiement = (TextView) findViewById(R.id.paiement);
            TextView setting = (TextView) findViewById(R.id.setting);
            TextView label_setting = (TextView) findViewById(R.id.label_setting);

            pwd_change = (TextView) findViewById(R.id.pwd_change);
            txt_nom = (TextView) findViewById(R.id.txt_nom);
            txt_mails = (TextView) findViewById(R.id.txt_mails);
            inpt_phone = (TextView) findViewById(R.id.inpt_phone);
            btn_carte = (TextView) findViewById(R.id.btn_carte);

            text.setTypeface(BoldCondensed);
            pwd_change.setTypeface(BoldCondensed);
            label_setting.setTypeface(RobotoBold);
            profil.setTypeface(RobotoBold);
            paiement.setTypeface(RobotoBold);
            setting.setTypeface(RobotoBold);
            btnDeconnect.setTypeface(RobotoBold);

            inpt_phone.setTypeface(RobotoMedium);
            txt_mails.setTypeface(RobotoMedium);
            txt_nom.setTypeface(RobotoMedium);
            btn_carte.setTypeface(RobotoMedium);


            loaderData();
        }
        else{
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_parametre));
            intent.putExtra("key", "close");
            startActivity(intent);
            finish();
        }*/
    }

    /*private void loaderData() {
        inpt_phone.setText(""+agent.getTelephone()+"");
        txt_mails.setText(agent.getQuatier());
        txt_nom.setText(agent.getNom()+" "+agent.getPrenoms());
        btn_carte.setText(agent.getDevice().getUuid());
    }*/


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
