package groupdigital.com.paiementcie.Activities.map;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Content.Dto.AddressDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.listener.Responses;
import groupdigital.com.paiementcie.Rest.response.ReponseOperator;
import groupdigital.com.paiementcie.Rest.response.ReponseUserInfo;
import groupdigital.com.paiementcie.Rest.service.Operator;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Locale;

public class MapsOfTaxPayerActivity extends AppCompatActivity implements Responses<ReponseOperator>, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener{
    private GoogleMap mMap;
    private TextView titre_sur_carte, info_position_sur_carte;
    private Button nextButon;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    private Bundle bundle;
    private Intent myIntent;
    private CameraUpdate cameraUpdate = null;
    private CameraPosition cameraPosition;
    private static final int DEFAULT_ZOOM = 12;
    private ProgressDialog nDialog;
    private double longitude, latitude;
    private MarkerOptions markerOptions;
    private Type type;
    private Geocoder geocoder;
    private List<Address> addresses;
    private LatLng latLng;
    private Gson gson;
    private String markOptions, taxPayerName, param;
    private AddressDto addressDto;
    private RelativeLayout realaTivAdressAriv, relativBtn;
    private Intent intent;
    private Typeface Material, RobotoBold, RobotoLight;
    private long taxPayerID;
    private Operator operator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maps_of_tax_payer);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        intent = getIntent();
        taxPayerName = intent.getStringExtra("taxpayerName");
        taxPayerID = intent.getLongExtra("ID", 0);

        //Log.i("OK--taxPayerID", String.valueOf(taxPayerID));
        nextButon = (Button) findViewById(R.id._btn_next_sur_carte);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.patient) + "...");
        nDialog.setCancelable(true);
        nDialog.setCanceledOnTouchOutside(true);
        nDialog.setIndeterminate(false);
        nDialog.show();

        addressDto = new AddressDto();
        markerOptions = new MarkerOptions();
        geocoder = new Geocoder(this, Locale.getDefault());

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
        RobotoLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        ImageView btnIntermediaire = (ImageView) findViewById(R.id.keyboardBack);
        btnIntermediaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
        initialiseMap();

        realaTivAdressAriv = (RelativeLayout) findViewById(R.id.info_reltiv);
        relativBtn = (RelativeLayout) findViewById(R.id.relativBtn);

        TextView titreHeader = (TextView) findViewById(R.id.titreHeader);
        titre_sur_carte = (TextView) findViewById(R.id.titre_sur_carte);
        info_position_sur_carte = (TextView) findViewById(R.id.info_position_sur_carte);

        titre_sur_carte.setTypeface(RobotoBold);
        info_position_sur_carte.setTypeface(RobotoLight);
        titreHeader.setTypeface(RobotoBold);
        nextButon.setTypeface(RobotoBold);


        nextButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (Connectivity.isConnected(MapsOfTaxPayerActivity.this)) {
                    nDialog.show();
                    takeListeOperator();
                }
                else{
                    intent = new Intent(MapsOfTaxPayerActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.no_network));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                }
            }
        });

    }

    private void initialiseMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view_search_carte);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng( 8.8515411, -2.841181),5.0f) );


        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }




    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    @Override
    public void onLocationChanged(Location location) {
        if(location != null){
            nDialog.dismiss();
            mLastLocation = location;
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            latitude = location.getLatitude(); longitude  = location.getLongitude();
            latLng = new LatLng(latitude, longitude);

            try {
                //Vérifie connection
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (Connectivity.isConnected(MapsOfTaxPayerActivity.this)) {
                    if (null != addresses && addresses.size() > 0) {
                        String lines = addresses.get(0).getAddressLine(0);
                        String subLocality = addresses.get(0).getSubLocality();
                        String region = addresses.get(0).getSubAdminArea();
                        if(subLocality != null){
                            markOptions = region + " - " + subLocality + " - "+lines ;
                        }
                        else{
                            markOptions = region + " - "+lines;
                        }
                        relativBtn.setVisibility(View.VISIBLE);
                        realaTivAdressAriv.setVisibility(View.VISIBLE);
                        nextButon.setVisibility(View.VISIBLE);
                        info_position_sur_carte.setText(markOptions);
                    }
                    else {
                        intent = new Intent(MapsOfTaxPayerActivity.this, AlertActivity.class);
                        intent.putExtra("msg", getResources().getString(R.string.no_network));
                        intent.putExtra("key", "close");
                        startActivity(intent);
                        markOptions = getResources().getString(R.string.impCord);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            /*addressDto.setLatitude(latitude);
            addressDto.setLongitude(longitude);
            addressDto.setAddressLine(markOptions);**/


            cameraPosition = new CameraPosition.Builder()
                    .target(latLng)
                    .zoom(DEFAULT_ZOOM)
                    .build();

            markerOptions.position(latLng);
            markerOptions.title(taxPayerName);
            markerOptions.draggable(true);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM));

            //stop location updates
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
    }



    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }




    private void takeListeOperator() {
        String mParam = Route.UPDATE_POSITION+"?id="+taxPayerID+"&latitude="+latitude+"&longitude="+longitude;
        operator = new Operator(this, mParam, this);
        operator.listOperator(mParam);
    }



    @Override
    public void getResponseList(ReponseOperator response) {
        nDialog.dismiss();
        intent = new Intent(MapsOfTaxPayerActivity.this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.success));
        intent.putExtra("key", "close");
        startActivity(intent);
        finish();

    }

    @Override
    public void OnRequestStartList() {

    }

    @Override
    public void OnRequestStopList() {

    }

    @Override
    public void onError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }
}
