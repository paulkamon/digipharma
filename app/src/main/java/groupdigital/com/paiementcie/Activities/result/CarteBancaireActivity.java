package groupdigital.com.paiementcie.Activities.result;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Alert.CongratulationActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.BankCardDto;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseInitialize;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseOPT;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.service.CardBankTask;
import groupdigital.com.paiementcie.Rest.service.InitializePaymentTask;
import groupdigital.com.paiementcie.Rest.service.PayRest;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;
import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.magnetic.MagneticCard;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CarteBancaireActivity extends Activity implements ResponseListener<ReponsePay> {
    private EditText codePin;
    private Button payCardBank;
    Handler handler;
    Thread readThread;
    private AlertDialog alert;
    private ProgressDialog nDialog;
    private Typeface Material, RobotoBold, RobotoMedium;
    private Intent intent;
    private TextView titreHeader;
    private List<FactureDto> listFacturePay;
    private UserInformationDto userDto;
    private String prefUser = Constants.PREF_NAME;
    private Collecteur collecteur;
    private Type type;
    private Gson gson;
    private String user,  vCodePin, vCardNumber;
    private int montantFacture;
    private RelativeLayout openCard, openFrm;
    private TextView titl1, comment;
    private InitializePaymentTask iTask;
    private BankCardDto bankCardDto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                , WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_carte_bancaire);

        alert = new AlertDialog.Builder(this).create();
        alert.setMessage(getResources().getString(R.string.readOK));
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);
        alert.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        readThread = new ReadThread();
                        readThread.start();
                    }
                });
        alert.show();

        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        if(user != null){
            collecteur = gson.fromJson(user, type);
        }

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.patient) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        intent = getIntent();
        montantFacture  = intent.getIntExtra("montantFacture", 0);
        listFacturePay  = (List<FactureDto>) intent.getSerializableExtra("listChoixFacture");
        userDto  = (UserInformationDto) intent.getSerializableExtra("userDto");

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
        RobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        openCard = (RelativeLayout) findViewById(R.id.openCard);
        openFrm = (RelativeLayout) findViewById(R.id.startOpen);

        //cardNumber = (EditText) findViewById(R.id.CardNumber);
        titl1 = (TextView) findViewById(R.id.titl1);
        comment = (TextView) findViewById(R.id.comment);


        handler = new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {

                openFrm.setVisibility(View.GONE);
                openCard.setVisibility(View.VISIBLE);
                payCardBank.setVisibility(View.VISIBLE);
                String[] TracData = (String[])msg.obj;
                vCardNumber = Constants.splitCard(TracData[1], "=",0).trim().substring(1);
                //Log.i("OK--vCardNumber", vCardNumber);
                /*cardNumber.setText("");
                cardNumber.setText(vCardNumber);
                cardOwner.setText(userDto.getNom() +" "+userDto.getPrenoms());
                montantBank.setText("Amount to be paid : "+montantFacture+" GHS");
                codeOTP.setVisibility(View.GONE);*/
            }
        };


        try {
            MagneticCard.open(CarteBancaireActivity.this);
        } catch (Exception e) {

            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.error_open_magnetic_card));
            intent.putExtra("key", "close");
            startActivity(intent);
        }

        settignsFrm();
    }

    private void settignsFrm()
    {
        /*phoneCustomer = (EditText) findViewById(R.id.phoneCustomer);
        validateCard = (EditText) findViewById(R.id.validateCard);
        cardCCV2 = (EditText) findViewById(R.id.cardCCV2);
        nameBank = (EditText) findViewById(R.id.nameBank);
        cardOwner = (EditText) findViewById(R.id.CardOwner);
        montantBank = (EditText) findViewById(R.id.montantBank);
        codeOTP = (EditText) findViewById(R.id.pwdAccountBank);*/

        titreHeader = (TextView)findViewById(R.id.titreHeader);
        codePin = (EditText) findViewById(R.id.codePin);
        payCardBank = (Button)findViewById(R.id._payCardBank);
        //startProcess = (Button)findViewById(R.id.startProcess);

        titreHeader.setTypeface(Material);
        titl1.setTypeface(RobotoBold);
        comment.setTypeface(RobotoMedium);
        codePin.setTypeface(RobotoBold);

        /*cardNumber.setTypeface(RobotoBold);
        validateCard.setTypeface(RobotoMedium);
        cardCCV2.setTypeface(RobotoMedium);
        nameBank.setTypeface(RobotoMedium);
        cardOwner.setTypeface(RobotoBold);
        montantBank.setTypeface(RobotoBold);
        codeOTP.setTypeface(RobotoMedium);*/

        payCardBank.setTypeface(RobotoBold);
        /*startProcess.setTypeface(RobotoBold);
        startProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                LauncheFirstStep();
            }
        });*/


        payCardBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finishProcesse();
            }
        });
    }

    protected void onDestroy() {
        if (readThread != null)
        {
            readThread.interrupt();
        }
        MagneticCard.close();
        super.onDestroy();
    }

    /*private void LauncheFirstStep() {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValidePin()){
                handlerProcess(vCodePin);
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }*/

    /*private boolean isValideDateCard()
    {
        String vLastNam = validateCard.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(validateCard);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vValidateCard = vLastNam;
            return true;
        }
    }

    private boolean isValidePhoneCustomer()
    {
        String vLastNam = phoneCustomer.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(validateCard);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vPhoneCustomer = vLastNam;
            return true;
        }
    }

    private boolean isValideCardVcv2()
    {
        String vLastNam = cardCCV2.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(cardCCV2);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vCardCCV2 = vLastNam;
            return true;
        }
    }

    private boolean isValideNameBank()
    {
        String vLastNam = nameBank.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(nameBank);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vNameBank = vLastNam.toUpperCase();
            return true;
        }
    }*/

    private boolean isValidePin()
    {
        String vLastNam = codePin.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(codePin);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vCodePin = vLastNam;
            return true;
        }
    }


    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /*private void handlerProcess(String vValidateCard, String codeCard,
            String vCodePin, String vNameBank, String phone)
    {
        bankCardDto = new BankCardDto();
        bankCardDto.setNumeroCarteBancaire(vCardNumber);
        bankCardDto.setBankName(vNameBank);
        bankCardDto.setValidityDate(vValidateCard);
        bankCardDto.setCodecard(codeCard);
        bankCardDto.setPin(vCodePin);
        bankCardDto.setAmount(montantFacture);
        bankCardDto.setNumeroMobile(phone);

        iTask = new InitializePaymentTask(this, Route.INITIALISE_PAYMENT_CARD_BANK, this);
        iTask.setResponseInitialize(this);
        iTask.initialiseCardBank(Route.INITIALISE_PAYMENT_CARD_BANK, bankCardDto);
    }


    @Override
    public void getResponseInitia(ReponseOPT retour) {
        nDialog.dismiss();
        try {
            if(retour.isStatus()){
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", getResources().getString(R.string.process_succes));
                intent.putExtra("key", "close");
                startActivity(intent);

                codePin.setEnabled(false);
                codePin.setInputType(InputType.TYPE_NULL);
                cardOwner.setEnabled(false);
                cardOwner.setInputType(InputType.TYPE_NULL);
                montantBank.setEnabled(false);
                montantBank.setInputType(InputType.TYPE_NULL);
                phoneCustomer.setEnabled(false);
                phoneCustomer.setInputType(InputType.TYPE_NULL);
                cardCCV2.setEnabled(false);
                cardCCV2.setInputType(InputType.TYPE_NULL);
                cardNumber.setEnabled(false);
                cardNumber.setInputType(InputType.TYPE_NULL);
                nameBank.setEnabled(false);
                nameBank.setInputType(InputType.TYPE_NULL);
                validateCard.setEnabled(false);
                validateCard.setInputType(InputType.TYPE_NULL);
                codeOTP.setVisibility(View.VISIBLE);
                codeOpt = retour.getData().getObject();
                codeOTP.setText(codeOpt);
                startProcess.setVisibility(View.GONE);
                payCardBank.setVisibility(View.VISIBLE);
            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", retour.getMessage());
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(validateCard);
            }
        } catch (Exception e){
            Log.i("OK--Exception", e.getMessage());
        }
    }

    @Override
    public void OntStartInitia() {

    }

    @Override
    public void OnStopInitia() {

    }

    @Override
    public void getErrorInitia() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
        requestFocus(validateCard);
    }*/

    private void finishProcesse() {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValidePin()){
                handlerFinishProcesse();
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void handlerFinishProcesse() {
        bankCardDto = new BankCardDto();
        bankCardDto.setNumeroCarteBancaire(vCardNumber);
        bankCardDto.setBankName("SGBCI");
        bankCardDto.setValidityDate("0319");
        bankCardDto.setCodecard("175");
        bankCardDto.setPin(vCodePin);
        bankCardDto.setAmount(montantFacture);
        bankCardDto.setNumeroMobile(userDto.getContact());
        bankCardDto.setUsername(collecteur.getUsername());
        bankCardDto.setIdContribuable(userDto.getId());
        bankCardDto.setIdModePaiement(4);
        bankCardDto.setFactureDto(listFacturePay);

        CardBankTask cardBankTask = new CardBankTask(this, Route.FINALISE_PAYMENT_BY_CARD_BANK, null);
        cardBankTask.setResponseListener(this);
        cardBankTask.FinalisePayment(Route.FINALISE_PAYMENT_BY_CARD_BANK, bankCardDto);
    }

    @Override
    public void getResponse(ReponsePay response) {
        nDialog.dismiss();
        try {
            if(response != null && response.isStatus()){
                finish();
                intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra("nRecu", response.getMessage());
                intent.putExtra("info",(ArrayList<FactureDto>)listFacturePay);
                intent.putExtra("userDto", userDto);
                intent.putExtra("typePay", "Card");
                startActivity(intent);
            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", response.getMessage());
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(codePin);
            }
        }catch (Exception e){
            Log.i("Ok--Error", e.getMessage());
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }

    private class ReadThread extends Thread
    {
        String[] TracData = null;
        @Override
        public void run()
        {
            MagneticCard.startReading();
            while (!Thread.interrupted()){
                try{
                    TracData = MagneticCard.check(1000);
                    handler.sendMessage(handler.obtainMessage(1, TracData));
                    MagneticCard.startReading();
                }catch (TelpoException e){
                }
            }
        }

    }


}

