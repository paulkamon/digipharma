package groupdigital.com.paiementcie.Activities.impression;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.BatteryManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Activities.flow.ModePayementActivity;
import groupdigital.com.paiementcie.Activities.home.TaxCollectActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.UsbThermalPrinter;
import com.telpo.tps550.api.util.StringUtil;
import com.telpo.tps550.api.util.SystemUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

public class ImprimerActivity extends Activity
{
    private Button print;

    private String printVersion, factContent;
    private final int NOPAPER = 3;
    private final int LOWBATTERY = 4;
    private final int PRINTVERSION = 5;
    private final int PRINTBARCODE = 6;
    private final int PRINTQRCODE = 7;
    private final int PRINTPAPERWALK = 8;
    private final int PRINTCONTENT = 9;
    private final int CANCELPROMPT = 10;
    private final int PRINTERR = 11;
    private final int OVERHEAT = 12;
    private final int MAKER = 13;
    private final int PRINTPICTURE = 14;
    private final int NOBLACKBLOCK = 15;

    MyHandler handler;

    private String Result;
    private Boolean nopaper = false;
    private boolean LowBattery = false;

    public static String barcodeStr;
    public static String qrcodeStr;
    public static int paperWalk;
    public static String printContent;
    private int leftDistance = 0;
    private int lineDistance, wordFont, printGray;
    private ProgressDialog progressDialog;
    private final static int MAX_LEFT_DISTANCE = 255;
    ProgressDialog dialog;
    UsbThermalPrinter mUsbThermalPrinter = new UsbThermalPrinter(ImprimerActivity.this);
    private String picturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/111.bmp";

    private Intent intent;
    private UserInformationDto userDto;
    private int size, sizeData, somme, somme1, mRestant;
    private Collecteur collecteur;
    private Gson gson;
    //    private String user, typePay, numRecu, paymentChoice, monaie, montantTotal;
    private Type type;
    private String prefUser = Constants.PREF_NAME;
    private Typeface Material, RobotoBold, RobotoLight;
    private List<FactureDto> factureDto;
    private PayFactureDto listFacture;

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            //Log.i("OKm", String.valueOf(msg.what));
            switch (msg.what) {
                case NOPAPER:
                    noPaperDlg();
                    break;
                case LOWBATTERY:
                    intent = new Intent(ImprimerActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.LowBattery));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                    break;
                case NOBLACKBLOCK:
                    intent = new Intent(ImprimerActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.maker_not_find));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                    break;
                case PRINTVERSION:
                    dialog.dismiss();
                    if (msg.obj.equals("1")) {
                        //textPrintVersion.setText(printVersion);
                    } else {
                        //Toast.makeText(ImprimerActivity.this, R.string.operation_fail, Toast.LENGTH_LONG).show();
                    }
                    break;
                case PRINTCONTENT:
                    new ImprimerActivity.contentPrintThread().start();
                    break;

                case CANCELPROMPT:
                    if (progressDialog != null && !ImprimerActivity.this.isFinishing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    break;
                case OVERHEAT:
                    intent = new Intent(ImprimerActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.overTemps));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                    break;
                case PRINTQRCODE:
                    Toast.makeText(ImprimerActivity.this, "print QRRRCODE", Toast.LENGTH_LONG).show();
                    break;
                case PRINTERR:
                    Toast.makeText(ImprimerActivity.this, "print errrorr", Toast.LENGTH_LONG).show();
                    break;
                default:
                    //Toast.makeText(ImprimerActivity.this, "Print Errorsssssss!", Toast.LENGTH_LONG).show();
                    intent = new Intent(ImprimerActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.nosupport));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                    break;
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_imprimer);

        savepic();

        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
//        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        handler = new MyHandler();

//        if(user != null){
//            collecteur = gson.fromJson(user, type);
//        }

        intent = getIntent();
//        typePay  = intent.getStringExtra("typePay");
//        numRecu  = intent.getStringExtra("numRecu");
        mRestant  = intent.getIntExtra("mRestant", 0);
        userDto  = (UserInformationDto) intent.getSerializableExtra("userDto");
//        if(typePay.equals("Card")){
//            paymentChoice = "Card";
//            monaie = "";
//        }
//        else if(typePay.equals("cb")){
//            paymentChoice = "Bank Accont";
//            monaie = "";
//        }
//        else if(typePay.equals("Momo")){
//            paymentChoice = "Mobile Money";
//            monaie = "";
//        }
//        else if(typePay.equals("cash")){
//            paymentChoice = "Cash";
//            if(mRestant == 0){
//                monaie = getResources().getString(R.string.change)+" 0 "+getResources().getString(R.string.devise);
//            }
//            else{
//                monaie = getResources().getString(R.string.change)+" "+mRestant+" "+getResources().getString(R.string.devise);
//            }
//        }
//        else if(typePay.equals("cheque")){
//            paymentChoice = "Check";
//            monaie = "";
//        }

        factContent = "";
//        if(typePay.equals("Card") || typePay.equals("Momo") || typePay.equals("cb")){
//            factureDto  = (List<FactureDto>)  intent.getSerializableExtra("factureDto");
//            size = factureDto.size();
//            if(size>0){
//                for(int i=0;i<size;i++){
//                    somme += factureDto.get(i).getMontant();
//                    factContent += "Invoice n°"+factureDto.get(i).getNumero()+" | "+factureDto.get(i).getDateFacture()+" : "+factureDto.get(i).getMontant()+"\n";
//                }
//                montantTotal = ""+somme +"";
//            }
//        }
//        else if(typePay.equals("cash") || typePay.equals("cheque")) {
//            listFacture  = (PayFactureDto) intent.getSerializableExtra("payFacture");
//            sizeData = listFacture.getFactureDto().size();
//            if(sizeData>0){
//                for(int i=0;i<sizeData;i++){
//                    somme1 += listFacture.getFactureDto().get(i).getMontant();
//                    factContent += "Invoice : "+listFacture.getFactureDto().get(i).getDateFacture()+" : "+listFacture.getFactureDto().get(i).getMontant()+"\n";
//                }
//                montantTotal = ""+somme1 +"";
//            }
//        }

        Material = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Black.ttf");
        RobotoLight = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Light.ttf");

        ImageView homeGo = (ImageView)findViewById(R.id.imageView2);
        homeGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutoLoad.startNewActivity(ImprimerActivity.this, TaxCollectActivity.class);
                finish();
            }
        });

        print = (Button)findViewById(R.id.print);
        TextView comment = (TextView)findViewById(R.id.comment);
        TextView titl1 = (TextView)findViewById(R.id.titl1);
        TextView headerTitle = (TextView)findViewById(R.id.titreHeader);

        headerTitle.setTypeface(Material);
        titl1.setTypeface(RobotoBold);
        print.setTypeface(RobotoBold);
        comment.setTypeface(RobotoLight);

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printContent
                        = "---------------------------\n" +
//                        "Date : "+ Constants.getDate() +"\n" +
//                        getResources().getString(R.string.code_received)+" : "+numRecu +"\n" +
//                        "Login : "+collecteur.getUsername()+"\n" +
//                        "---------------------------\n" +
//                        getResources().getString(R.string.user)+" : "+userDto.getNom()+" "+userDto.getPrenoms()+"\n" +
//                        getResources().getString(R.string.tel_)+" : "+userDto.getContact()+"\n" +
//                        getResources().getString(R.string.id)+" : "+userDto.getNumero() +"\n" +
//                        "---------------------------\n"+
//                        "*"+getResources().getString(R.string.fac_pay)+" :"+
//                        ""+factContent+
//                        "---------------------------\n"+
//                        "*"+getResources().getString(R.string.total)+" : "+montantTotal+" "+getResources().getString(R.string.devise)+"\n" +
//                        "---------------------------\n"+
//                        getResources().getString(R.string.type_mode_paiement)+" : "+paymentChoice+"\n" +
//                        ""+monaie+"\n" +
                        "---------------------------\n";
                String exditText;
                exditText ="0"; //editTextLeftDistance.getText().toString();
                if (exditText == null || exditText.length() < 1) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.left_margin) + getString(R.string.lengthNotEnougth), Toast.LENGTH_LONG).show();
                    return;
                }
                leftDistance = Integer.parseInt(exditText);
                exditText = "0";//editTextLineDistance.getText().toString();
                if (exditText == null || exditText.length() < 1) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.row_space) + getString(R.string.lengthNotEnougth), Toast.LENGTH_LONG).show();
                    return;
                }
                lineDistance = Integer.parseInt(exditText);
                //printContent = editTextContent.getText().toString();
                exditText = "2";//editTextWordFont.getText().toString();
                if (exditText == null || exditText.length() < 1) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.font_size) + getString(R.string.lengthNotEnougth), Toast.LENGTH_LONG).show();
                    return;
                }
                wordFont = Integer.parseInt(exditText);
                exditText ="3"; //editTextPrintGray.getText().toString();
                if (exditText == null || exditText.length() < 1) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.gray_level) + getString(R.string.lengthNotEnougth), Toast.LENGTH_LONG).show();
                    return;
                }
                printGray = Integer.parseInt(exditText);
                if (leftDistance > MAX_LEFT_DISTANCE) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.outOfLeft), Toast.LENGTH_LONG).show();
                    return;
                }
                if (lineDistance > 255) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.outOfLine), Toast.LENGTH_LONG).show();
                    return;
                }
                if (wordFont > 4 || wordFont < 1) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.outOfFont), Toast.LENGTH_LONG).show();
                    return;
                }
                if (printGray < 0 || printGray > 7) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.outOfGray), Toast.LENGTH_LONG).show();
                    return;
                }
                if (printContent == null || printContent.length() == 0) {
                    //Toast.makeText(ImprimerActivity.this, getString(R.string.empty), Toast.LENGTH_LONG).show();
                    return;
                }
                if (LowBattery == true) {
                    handler.sendMessage(handler.obtainMessage(LOWBATTERY, 1, 0, null));
                    //Log.i("OK","xxxx");
                } else {
                    if (!nopaper) {
                        //Log.i("OK","xxkk");
                        progressDialog = ProgressDialog.show(
                                ImprimerActivity.this,
                                getString(R.string.app_name),
                                getString(R.string.printing_wait));
                        handler.sendMessage(handler.obtainMessage(PRINTCONTENT, 1, 0, null));

                    } else {
                        //Log.i("OK","xxkkwww");
                        Toast.makeText(ImprimerActivity.this, getString(R.string.ptintInit), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        IntentFilter pIntentFilter = new IntentFilter();
        pIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        pIntentFilter.addAction("android.intent.action.BATTERY_CAPACITY_EVENT");
        registerReceiver(printReceive, pIntentFilter);

        dialog = new ProgressDialog(ImprimerActivity.this);
        dialog.setMessage(getText(R.string.watting));
        dialog.setCancelable(false);
        dialog.show();

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mUsbThermalPrinter.start(0);
                    mUsbThermalPrinter.reset();
                    printVersion = mUsbThermalPrinter.getVersion();
                } catch (TelpoException e) {
                    e.printStackTrace();
                } finally {
                    if (printVersion != null) {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "1";
                        handler.sendMessage(message);
                    } else {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "0";
                        handler.sendMessage(message);
                    }
                }
            }
        }).start();
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onDestroy() {
        if (progressDialog != null && !ImprimerActivity.this.isFinishing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        unregisterReceiver(printReceive);
        mUsbThermalPrinter.stop();
        super.onDestroy();
    }

    private final BroadcastReceiver printReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_NOT_CHARGING);
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
                //TPS390 can not print,while in low battery,whether is charging or not charging
                if(SystemUtil.getDeviceType() == StringUtil.DeviceModelEnum.TPS390.ordinal()){
                    if (level * 5 <= scale) {
                        LowBattery = true;
                    } else {
                        LowBattery = false;
                    }
                }else {
                    if (status != BatteryManager.BATTERY_STATUS_CHARGING) {
                        if (level * 5 <= scale) {
                            LowBattery = true;
                        } else {
                            LowBattery = false;
                        }
                    } else {
                        LowBattery = false;
                    }
                }
            }
            //Only use for TPS550MTK devices
            else if (action.equals("android.intent.action.BATTERY_CAPACITY_EVENT")) {
                int status = intent.getIntExtra("action", 0);
                int level = intent.getIntExtra("level", 0);
                if(status == 0){
                    if(level < 1){
                        LowBattery = true;
                    }else {
                        LowBattery = false;
                    }
                }else {
                    LowBattery = false;
                }
            }
        }
    };

    private void noPaperDlg() {
        intent = new Intent(ImprimerActivity.this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.noPaperNotice));
        intent.putExtra("key", "close");
        startActivity(intent);
    }

    private class contentPrintThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                mUsbThermalPrinter.reset();
                mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);
                mUsbThermalPrinter.setLeftIndent(leftDistance);
                mUsbThermalPrinter.setLineSpace(lineDistance);
                if (wordFont == 4) {
                    mUsbThermalPrinter.setFontSize(2);
                    mUsbThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 3) {
                    mUsbThermalPrinter.setFontSize(1);
                    mUsbThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 2) {
                    mUsbThermalPrinter.setFontSize(2);
                } else if (wordFont == 1) {
                    mUsbThermalPrinter.setFontSize(1);
                }
                mUsbThermalPrinter.setGray(printGray);
                File file = new File(picturePath);
                if (file.exists()) {
                    mUsbThermalPrinter.printLogo(BitmapFactory.decodeFile(picturePath),false);
                    mUsbThermalPrinter.walkPaper(2);
                }
                mUsbThermalPrinter.addString(printContent);
                mUsbThermalPrinter.printString();
                mUsbThermalPrinter.walkPaper(20);
                //finish();
            } catch (Exception e) {
                e.printStackTrace();
                Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                } else {
                    handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                }
                System.out.println(e.toString());
            } finally {
                handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                if (nopaper){
                    handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                    nopaper = false;
                    return;
                }
            }
        }
    }

    private void savepic() {
        File file = new File(picturePath);
        if (!file.exists()) {
            InputStream inputStream = null;
            FileOutputStream fos = null;
            byte[] tmp = new byte[1024];
            try {
                inputStream = getApplicationContext().getAssets().open("dat3.png");
                fos = new FileOutputStream(file);
                int length = 0;
                while((length = inputStream.read(tmp)) > 0){
                    fos.write(tmp, 0, length);
                }
                fos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
