package groupdigital.com.paiementcie.Activities.result;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Alert.CongratulationActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.BankPaymentDto;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseInitialize;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseOPT;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.service.BankTask;
import groupdigital.com.paiementcie.Rest.service.InitializePaymentTask;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CompteBancaireActivity extends AppCompatActivity
    implements ResponseInitialize<ReponseOPT>, ResponseListener<ReponsePay> {

    private EditText numeroAccountBank, pwdAccountBank, montantBank;
    private Button payByBank, btnInitializePayment;
    private ProgressDialog nDialog;
    private Typeface Material, RobotoBold;
    private Intent intent;
    private TextView titreHeader;
    private int montantFacture;
    private List<FactureDto> listFacturePay;
    private UserInformationDto userDto;
    private String prefUser = Constants.PREF_NAME;
    private Collecteur collecteur;
    private PayFactureDto payFactureDto;
    private String user, NumeroAccountBank, codeOpt, vCodeOPT;
    private Type type;
    private Gson gson;
    private BankPaymentDto bankPayment;
    private InitializePaymentTask iTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_compte_bancaire);

        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        if(user != null){
            collecteur = gson.fromJson(user, type);
        }

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.patient) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        intent = getIntent();
        montantFacture  = intent.getIntExtra("montantFacture", 0);
        listFacturePay  = (List<FactureDto>) intent.getSerializableExtra("listChoixFacture");
        userDto  = (UserInformationDto) intent.getSerializableExtra("userDto");

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        configuration();

        ImageView keyboard = (ImageView)findViewById(R.id.keyboardBackBank);
        keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void configuration() {
        numeroAccountBank = (EditText) findViewById(R.id.numeroAccountBank);
        pwdAccountBank = (EditText) findViewById(R.id.pwdAccountBank);
        montantBank = (EditText) findViewById(R.id.montantBank);
        titreHeader = (TextView)findViewById(R.id.titreHeader);
        payByBank = (Button)findViewById(R.id._payAccountBank);
        btnInitializePayment = (Button)findViewById(R.id.initialise);

        titreHeader.setTypeface(Material);
        pwdAccountBank.setTypeface(RobotoBold);
        numeroAccountBank.setTypeface(RobotoBold);
        montantBank.setTypeface(RobotoBold);

        btnInitializePayment.setTypeface(RobotoBold);
        payByBank.setTypeface(RobotoBold);

        montantBank.setText(getResources().getString(R.string.amount_to_be_paid)+" "+montantFacture+" "+getResources().getString(R.string.devise));

        btnInitializePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startProcess();
            }
        });

        payByBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finishProcesse();
            }
        });
    }


    private void finishProcesse() {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValideNumeroAccountBank() && isValideCodeOPT()){
                if(vCodeOPT.contentEquals(codeOpt)){
                    handlerFinishProcesse();
                }
                else{
                    nDialog.dismiss();
                    intent = new Intent(this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.bad_opt));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                    requestFocus(numeroAccountBank);
                }
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }


    private void startProcess()
    {
        nDialog.show();
        if (Connectivity.isConnected(this)) {
            if(isValideNumeroAccountBank()){
                handlerProcess(NumeroAccountBank);
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void handlerProcess(String numeroAccountBank)
    {
        bankPayment = new BankPaymentDto();
        //bankPayment.setBankName(nameBank);
        bankPayment.setCompteBancaire(numeroAccountBank);
        //bankPayment.setNumeroMobile(phone);
        bankPayment.setAmount(montantFacture);

        iTask = new InitializePaymentTask(this, Route.INITIALISE_PAYMENT_BANK, this);
        iTask.setResponseInitialize(this);
        iTask.initialiseProcessBank(Route.INITIALISE_PAYMENT_BANK, bankPayment);
    }

    private boolean isValideNumeroAccountBank()
    {
        String vLastNam = numeroAccountBank.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(numeroAccountBank);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            NumeroAccountBank = vLastNam;
            return true;
        }
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void getResponseInitia(ReponseOPT retour)
    {
        nDialog.dismiss();
        try {
            if(retour.isStatus()){
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", getResources().getString(R.string.process_succes));
                intent.putExtra("key", "close");
                startActivity(intent);

                numeroAccountBank.setEnabled(false);
                numeroAccountBank.setInputType(InputType.TYPE_NULL);
                pwdAccountBank.setVisibility(View.VISIBLE);
                codeOpt = retour.getData().getObject();

                btnInitializePayment.setVisibility(View.GONE);
                payByBank.setVisibility(View.VISIBLE);
                requestFocus(pwdAccountBank);
            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", retour.getMessage());
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(numeroAccountBank);
            }
        } catch (Exception e){
            Log.i("OK--Exception", e.getMessage());
        }
    }

    @Override
    public void OntStartInitia() {

    }

    @Override
    public void OnStopInitia() {

    }

    @Override
    public void getErrorInitia() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
        requestFocus(numeroAccountBank);
    }

    private void handlerFinishProcesse()
    {
        BankPaymentDto dto = new BankPaymentDto();
        dto.setOtp(codeOpt);
        dto.setCompteBancaire(bankPayment.getCompteBancaire());
        dto.setGetOperatorId(bankPayment.getGetOperatorId());
        dto.setAmount(bankPayment.getAmount());
        dto.setUsername(collecteur.getUsername());
        dto.setIdContribuable(userDto.getId());
        dto.setIdModePaiement(5);
        dto.setFactureDto(listFacturePay);

        BankTask bankTask = new BankTask(this, Route.FINISH_PAYMENT_BANK, null);
        bankTask.setResponseListener(this);
        bankTask.FinalisePayment(Route.FINISH_PAYMENT_BANK, dto);
    }

    @Override
    public void getResponse(ReponsePay response) {
        nDialog.dismiss();
        try {
            if(response != null && response.isStatus()){
                finish();
                intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra("nRecu", response.getMessage());
                intent.putExtra("info", (ArrayList<FactureDto>) listFacturePay);
                intent.putExtra("userDto", userDto);
                intent.putExtra("typePay", "cb");
                startActivity(intent);
            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", response.getMessage());
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(numeroAccountBank);
            }
        }catch (Exception e){
            Log.i("Ok--Error", e.getMessage());
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }

    private boolean isValideCodeOPT()
    {
        String vLastNam = pwdAccountBank.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(pwdAccountBank);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            vCodeOPT = vLastNam;
            return true;
        }
    }
}
