package groupdigital.com.paiementcie.Activities.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Activities.impression.ImprimerBilanActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Adapter.AdapterBilan;
import groupdigital.com.paiementcie.Content.Dto.BilanJournalierDto;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponseBilan;
import groupdigital.com.paiementcie.Rest.service.BilanService;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import static java.lang.Thread.sleep;

public class BilanJournalierActivity extends AppCompatActivity implements ResponseListener<ReponseBilan> {

    private Typeface Material, RobotoBold, RobotoLight;
    private TextView headerTitle, comment, titl1, comment2, titl2, montantTotal, btnImprimer, dateJour;
    private ProgressDialog nDialog;
    private Gson gson;
    private BilanService bilanService;
    private List<BilanJournalierDto> bilanJournalierDto = new ArrayList<BilanJournalierDto>();
    private Collecteur collecteur;
    private String user;
    private Type type;
    private Handler mHandler;
    private RelativeLayout dataExist, problemServer, dataNoExist;
    private RecyclerView myListe;
    private FastScroller fasterList;
    public AdapterBilan adapter;
    private String prefUser = Constants.PREF_NAME;
    private long somme = 0, totalReport = 0;
    private String datPrint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_bilan_journalier);

        boolean paulImprime = AutoLoad.getPrefs().getBoolean(Constants.PREF_PRINT, false);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.loading) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        //Initialisation
        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        mHandler = new Handler();

        if(user != null){
            collecteur = gson.fromJson(user, type);
        }


        montantTotal = (TextView)findViewById(R.id.total);
        btnImprimer = (TextView)findViewById(R.id.imprimer);
        headerTitle = (TextView)findViewById(R.id.titreHeader);
        comment = (TextView)findViewById(R.id.comment);
        dateJour = (TextView)findViewById(R.id.dateJour);
        comment2 = (TextView)findViewById(R.id.comment2);
        titl2 = (TextView)findViewById(R.id.titl2);
        titl1 = (TextView)findViewById(R.id.titl1);

        Material = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Black.ttf");
        RobotoLight = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Light.ttf");

        headerTitle.setTypeface(Material);
        comment.setTypeface(RobotoLight);
        comment2.setTypeface(RobotoLight);
        titl1.setTypeface(RobotoBold);
        titl2.setTypeface(RobotoBold);
        dateJour.setTypeface(RobotoBold);
        montantTotal.setTypeface(RobotoBold);
        btnImprimer.setTypeface(RobotoBold);


        btnImprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printReport();
            }
        });

        //Recupératon de la pref print
        if(paulImprime){
            btnImprimer.setVisibility(View.VISIBLE);
        }

        dataExist = (RelativeLayout) findViewById(R.id.dataExist);
        problemServer = (RelativeLayout) findViewById(R.id.problemServer);
        dataNoExist = (RelativeLayout) findViewById(R.id.dataNoExist);

        if(collecteur != null){
            LaunchRest();
        }

        ImageView imgClose = (ImageView)findViewById(R.id.imageView2);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initializesDatas();
    }

    private void LaunchRest() {
        if (Connectivity.isConnected(this)) {
            nDialog.show();
            String mParam = Route.BILAN_JOURNALIER+"?username="+collecteur.getUsername();
            bilanService = new BilanService(this, mParam, this);
            bilanService.findAll(mParam);
        }
        else{
            problemServer.setVisibility(View.VISIBLE);
        }
    }

    private void initializesDatas() {
        myListe = (RecyclerView) findViewById(R.id.myListe);
        fasterList = (FastScroller) findViewById(R.id.fasterList);
        myListe.setHasFixedSize(true);
        myListe.setLayoutManager(new LinearLayoutManager(BilanJournalierActivity.this));
        adapter = new AdapterBilan(this, bilanJournalierDto);
        myListe.setAdapter(adapter);
        fasterList.setRecyclerView(myListe);
    }



    @Override
    public void getResponse(ReponseBilan response) {
        try{
            if(response != null){
                nDialog.dismiss();
                if(response.getData().size() == 0){
                    dataNoExist .setVisibility(View.VISIBLE);
                }
                else if(response.getData().size() > 0){
                    datPrint = response.getMessage();
                    dateJour.setText("The date of : "+response.getMessage());
                    problemServer.setVisibility(View.GONE);
                    dataExist .setVisibility(View.VISIBLE);
                    if(response!= null && response.isStatus()){
                        int sizeData = response.getData().size();
                        if(sizeData > 0){
                            dataExist .setVisibility(View.VISIBLE);
                            for(int i=0;i<sizeData;i++){
                                somme += response.getData().get(i).getMontantPreleve();
                                bilanJournalierDto.add(
                                    new BilanJournalierDto(
                                        response.getData().get(i).getMontantPreleve(),
                                        response.getData().get(i).getNom(),
                                        response.getData().get(i).getModePaiement()
                                    )
                                );
                            }
                            totalReport = somme;
                            montantTotal.setText("Total : "+somme +" GHS");
                            adapter.notifyDataSetChanged();
                            OnRequestStop();
                        }
                        else if(sizeData == 0){
                            dataNoExist .setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
        catch (Exception e){
            nDialog.dismiss();
            problemServer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        problemServer .setVisibility(View.VISIBLE);
    }

    private void printReport() {
        Intent intent = new Intent(this, ImprimerBilanActivity.class);
        intent.putExtra("date", datPrint);
        intent.putExtra("report", (Serializable) bilanJournalierDto);
        startActivity(intent);
        finish();
    }
}
