package groupdigital.com.paiementcie.Activities.home.Agent;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.R;

import java.lang.reflect.Type;

import de.hdodenhof.circleimageview.CircleImageView;

public class AgentActivity extends AppCompatActivity {

    //private Agent agent;
    private String agentConnect;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent);
        /*Gson gson = new Gson();
        Type type = new TypeToken<Agent>() {}.getType();
        agentConnect = AutoLoad.getPrefs().getString(Constants.PREF_NAME, null);
        agent = gson.fromJson(agentConnect, type);
        if (agentConnect != null) {
            setContentView(R.layout.activity_agent);

            ImageView bClose = (ImageView) findViewById(R.id.fermer_cmpte);
            bClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            getDataOfPref();
        }
        else{
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_parametre));
            intent.putExtra("key", "close");
            startActivity(intent);
            finish();
        }*/
    }

    /*private void getDataOfPref() {
        Typeface RobotoCondensed = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        Typeface RobotoBlack = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Black.ttf");
        Typeface RobotoLight = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Light.ttf");

        TextView headerTitle = (TextView)findViewById(R.id.titleClose);
        TextView profil = (TextView)findViewById(R.id.profil);
        TextView organ = (TextView)findViewById(R.id.organ);
        headerTitle.setTypeface(RobotoCondensed);
        profil.setTypeface(RobotoBlack);
        organ.setTypeface(RobotoBlack);


        //CircleImageView pp_agent = (CircleImageView)findViewById(R.id.pp_agent);
        TextView txt_organisation = (TextView)findViewById(R.id.txt_organisation);
        TextView txt_adress_org = (TextView)findViewById(R.id.txt_adress_org);
        TextView txt_nom = (TextView)findViewById(R.id.txt_nom);
        TextView matricule = (TextView)findViewById(R.id.txt_mails);
        TextView inpt_phone = (TextView)findViewById(R.id.inpt_phone);
        TextView inpt_dateLieu = (TextView)findViewById(R.id.inpt_dateLieu);
        TextView inpt_quartier = (TextView)findViewById(R.id.inpt_quartier);

        txt_organisation.setTypeface(RobotoLight);
        txt_adress_org.setTypeface(RobotoLight);
        txt_nom.setTypeface(RobotoLight);
        matricule.setTypeface(RobotoLight);
        inpt_phone.setTypeface(RobotoLight);
        inpt_quartier.setTypeface(RobotoLight);
        inpt_dateLieu.setTypeface(RobotoLight);

        txt_nom.setText(agent.getNom() +" "+agent.getPrenoms());
        inpt_phone.setText(getResources().getString(R.string.tel)+" "+agent.getTelephone());
        inpt_dateLieu.setText(getResources().getString(R.string.nele)+" "+agent.getDateNaiss());
        matricule.setText(getResources().getString(R.string.mat)+" "+agent.getMatricule());
        inpt_quartier.setText(agent.getQuatier());
        txt_adress_org.setText(agent.getOrganisation().getTelephone());
        txt_organisation.setText(agent.getOrganisation().getLibelle());

    }*/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
