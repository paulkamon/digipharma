package groupdigital.com.paiementcie.Activities.result;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import groupdigital.com.paiementcie.Activities.flow.ModePayementActivity;
import groupdigital.com.paiementcie.Activities.flow.UserInforamtionActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.Alert.CongratulationActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Dto.Collecteur;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.PayFactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ResponseListener;
import groupdigital.com.paiementcie.Rest.response.ReponsePay;
import groupdigital.com.paiementcie.Rest.service.PayRest;
import groupdigital.com.paiementcie.Utilitaire.Connectivity;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class CashActivity extends AppCompatActivity implements ResponseListener<ReponsePay> {

    private ProgressDialog nDialog;
    private Typeface Material, RobotoBold;
    private Intent intent;
    private TextView titreHeader;
    private int montantFacture, accountGiven, rendueSom;
    private EditText montantcash, change, givenAccount;
    private String prefUser = Constants.PREF_NAME;
    private Collecteur collecteur;
    private PayFactureDto payFactureDto;
    private String user;
    private Type type;
    private Gson gson;
    private Button payCash;
    private List<FactureDto> listFacturePay;
    private UserInformationDto userDto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cash);

        gson = new Gson();
        type = new TypeToken<Collecteur>() {}.getType();
        user = AutoLoad.getPrefs().getString(this.prefUser, null);
        if(user != null){
            collecteur = gson.fromJson(user, type);
        }

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getResources().getString(R.string.patient) + "...");
        nDialog.setCancelable(false);
        nDialog.setCanceledOnTouchOutside(false);
        nDialog.setIndeterminate(false);

        intent = getIntent();
        montantFacture  = intent.getIntExtra("montantFacture", 0);
        listFacturePay  = (List<FactureDto>) intent.getSerializableExtra("listChoixFacture");
        userDto  = (UserInformationDto) intent.getSerializableExtra("userDto");


        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");

        givenAccount = (EditText)findViewById(R.id.givenAccount);
        change = (EditText)findViewById(R.id.Change);
        montantcash = (EditText)findViewById(R.id.montantcash);
        payCash = (Button)findViewById(R.id._payCash);

        titreHeader = (TextView)findViewById(R.id.titreHeader);
        titreHeader.setTypeface(Material);

        montantcash.setTypeface(RobotoBold);
        change.setTypeface(RobotoBold);
        givenAccount.setTypeface(RobotoBold);
        payCash.setTypeface(RobotoBold);

        montantcash.setText(getResources().getString(R.string.amount_to_be_paid)+" "+montantFacture+" "+getResources().getString(R.string.devise));

        givenAccount.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")){
                    change.setText("");
                }else{
                    int value = Integer.parseInt(s.toString());
                    if(value > montantFacture || value == 0){
                        rendueSom = value - montantFacture;
                        change.setText(getResources().getString(R.string.change)+" "+rendueSom+" "+getResources().getString(R.string.devise));
                    }
                    else if(value < montantFacture){
                        change.setText("");
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        ImageView keyboard = (ImageView)findViewById(R.id.fermer_cmpte);
        keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        payCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rendueSom == 0 || rendueSom > 0){
                    if(collecteur != null){
                        nDialog.show();
                        PayInvoiceCash();
                    }
                }
                else{
                    nDialog.dismiss();
                    intent = new Intent(CashActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.unauthorized));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                }
            }
        });
    }

    private void PayInvoiceCash() {
        if (Connectivity.isConnected(this)) {
            if(isValideAccount()){
                if(montantFacture > accountGiven){
                    intent = new Intent(CashActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.unthorized));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                }
                else{
                    handlerPayAccount();
                }
            }
        }
        else{
            nDialog.dismiss();
            intent = new Intent(CashActivity.this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.no_network));
            intent.putExtra("key", "close");
            startActivity(intent);
        }
    }

    private void handlerPayAccount() {
        payFactureDto = new PayFactureDto();
        payFactureDto.setUsername(collecteur.getUsername());
        payFactureDto.setIdContribuable(userDto.getId());
        payFactureDto.setMontantFacture(montantFacture);
        payFactureDto.setSommeRecu(accountGiven);
        payFactureDto.setSommeRestante(rendueSom);
        payFactureDto.setIdModePaiement(1);
        payFactureDto.setFactureDto(listFacturePay);

        PayRest payRest = new PayRest(this, Route.PAYMENT_FACTURE_CASH_CHEQUE, null);
        payRest.setResponseListener(this);
        payRest.payment(Route.PAYMENT_FACTURE_CASH_CHEQUE, payFactureDto);
    }


    private boolean isValideAccount()
    {
        String vLastNam = givenAccount.getText().toString().trim();
        if(vLastNam.isEmpty() || vLastNam == ""){
            nDialog.dismiss();
            requestFocus(givenAccount);
            intent = new Intent(this, AlertActivity.class);
            intent.putExtra("msg", getResources().getString(R.string.requis));
            intent.putExtra("key", "close");
            startActivity(intent);
            return false;
        }
        else{
            accountGiven = Integer.parseInt(vLastNam);
            return true;
        }
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void getResponse(ReponsePay response) {
        nDialog.dismiss();
        try {
            if(response != null && response.isStatus()){
                finish();
                intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra("nRecu", response.getMessage());
                intent.putExtra("info", payFactureDto);
                intent.putExtra("rendueSom", rendueSom);
                intent.putExtra("userDto", userDto);
                intent.putExtra("typePay", "cash");
                startActivity(intent);
            }
            else{
                intent = new Intent(this, AlertActivity.class);
                intent.putExtra("msg", response.getMessage());
                intent.putExtra("key", "close");
                startActivity(intent);
                requestFocus(givenAccount);
            }
        }catch (Exception e){
            Log.i("Ok--Error", e.getMessage());
        }
    }

    @Override
    public void OnRequestStart() {

    }

    @Override
    public void OnRequestStop() {

    }

    @Override
    public void getError() {
        nDialog.dismiss();
        intent = new Intent(this, AlertActivity.class);
        intent.putExtra("msg", getResources().getString(R.string.impossible));
        intent.putExtra("key", "close");
        startActivity(intent);
    }
}
