package groupdigital.com.paiementcie.Activities.splash;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import groupdigital.com.paiementcie.Activities.LoginActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Utilitaire.Constants;
import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.magnetic.MagneticCard;
import com.telpo.tps550.api.printer.UsbThermalPrinter;

import static java.lang.Thread.sleep;

public class SplashActivity extends AppCompatActivity {

    private String printVersion, factContent;
    private final int PRINTVERSION = 5;
    private splashHandler handler;
    private Thread readThread;
    private final int DUREE_SPLASH = 2 * 1000;// 4 seconds
    private boolean showRationale = true;
    private boolean hasGrantedPermissions = true;
    private String Result, jsonIP, jsonPort;
    private Boolean nopaper = false;
    private boolean LowBattery = false;

    public static int paperWalk, barcodeStr, qrcodeStr;
    public static String printContent;
    private int leftDistance = 0;
    private int lineDistance, wordFont, printGray;
    private ProgressDialog progressDialog;
    private final static int MAX_LEFT_DISTANCE = 255;
    ProgressDialog dialog;
    UsbThermalPrinter mUsbThermalPrinter = new UsbThermalPrinter(this);
    private String picturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/111.bmp";

    private String prefSetting = Constants.PREF_CONFIG;
    private boolean ParamDevice;

    private SplashActivity mActivity;
    private static final int REQUEST_PERMISSIONS = 100;
    private static final String PERMISSIONS_REQUIRED[] = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.CHANGE_WIFI_STATE,
    };
    private LocationManager locationManager ;
    private Context context;
    boolean GpsStatus ;

    private class splashHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            //Log.i("OK--WHAT", String.valueOf(msg.what));
            switch (msg.what) {
                case PRINTVERSION:
                    if (msg.obj.equals("1")) {
                        AutoLoad.getPrefs().putBoolean(Constants.PREF_PRINT, true);
                    } else {
                        AutoLoad.getPrefs().putBoolean(Constants.PREF_PRINT, false);
                    }
                    verifSmartCard();
                break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mActivity = this;
        handler = new splashHandler();
        setContentView(R.layout.activity_splash);

        context = getApplicationContext();
        ParamDevice  = AutoLoad.getPrefs().getBoolean(this.prefSetting, false);
        locationManager = (LocationManager)this.getSystemService(this.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(GpsStatus)
        {
            checkPermissions();
        }
        else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setMessage(getResources().getString(R.string.gps));
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent1);
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            System.exit(0);
                        }
                    });
            alertDialog.show();
        }
    }

    private void verifNFC() {
        NfcManager nfcManager = (NfcManager) this.getSystemService(Context.NFC_SERVICE);
        NfcAdapter nfcAdapter = nfcManager.getDefaultAdapter();
        if (nfcAdapter == null) {
            AutoLoad.getPrefs().putBoolean(Constants.PREF_NFC, true);
        }
        else {
            AutoLoad.getPrefs().putBoolean(Constants.PREF_NFC, false);
        }
        writeRemeber();
    }

    private void verifSmartCard() {
        try {
            AutoLoad.getPrefs().putBoolean(Constants.PREF_SMARTCART, true);
            MagneticCard.open(SplashActivity.this);
        } catch (TelpoException e) {
            e.printStackTrace();
            AutoLoad.getPrefs().remove(Constants.PREF_SMARTCART);
            AutoLoad.getPrefs().putBoolean(Constants.PREF_SMARTCART, false);
        }
        verifNFC();
    }


    private void verifPrint() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mUsbThermalPrinter.start(0);
                    mUsbThermalPrinter.reset();
                    printVersion = mUsbThermalPrinter.getVersion();
                } catch (TelpoException e) {
                    e.printStackTrace();
                } finally {
                    if (printVersion != null) {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "1";
                        handler.sendMessage(message);
                    } else {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "0";
                        handler.sendMessage(message);
                    }
                }
            }
        }).start();
    }

    private class ReadThread extends Thread
    {
        @Override
        public void run()
        {
            MagneticCard.startReading();
        }

    }

    private void writeRemeber() {
        AutoLoad.getPrefs().putBoolean(Constants.PREF_CONFIG, true);
        AutoLoad.startNewActivity(SplashActivity.this, LoginActivity.class);
        finish();
    }

    private boolean checkPermission(String permissions[]) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    private void checkPermissions()
    {
        boolean permissionsGranted = checkPermission(PERMISSIONS_REQUIRED);
        //Log.i("OK---permissionsGranted", String.valueOf(permissionsGranted));
        if (permissionsGranted) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        sleep(DUREE_SPLASH);
                        if(ParamDevice){
                            AutoLoad.startNewActivity(SplashActivity.this, LoginActivity.class);
                            finish();
                        }
                        else{
                            readThread = new ReadThread();
                            readThread.start();
                            verifPrint();
                        }
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
        else {
            for (String permission: PERMISSIONS_REQUIRED) {
                showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                if (!showRationale) {
                    break;
                }
            }
            ActivityCompat.requestPermissions(mActivity, PERMISSIONS_REQUIRED, REQUEST_PERMISSIONS);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS) {
            for (int i=0; i<grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    hasGrantedPermissions = false;
                    break;
                }
            }

            //il accepte toute les permission
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        sleep(DUREE_SPLASH);
                        if(ParamDevice){
                            AutoLoad.startNewActivity(SplashActivity.this, LoginActivity.class);
                            finish();
                        }
                        else{
                            readThread = new ReadThread();
                            readThread.start();
                            verifPrint();
                        }
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
        else {
            finish();
        }
    }

}
