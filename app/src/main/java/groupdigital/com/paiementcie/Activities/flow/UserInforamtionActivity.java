package groupdigital.com.paiementcie.Activities.flow;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.futuremind.recyclerviewfastscroll.FastScroller;
import groupdigital.com.paiementcie.Activities.LoginActivity;
import groupdigital.com.paiementcie.Activities.home.CollectActivity;
import groupdigital.com.paiementcie.Activities.home.TaxCollectActivity;
import groupdigital.com.paiementcie.Activities.map.MapsActivity;
import groupdigital.com.paiementcie.Activities.map.MapsOfTaxPayerActivity;
import groupdigital.com.paiementcie.Activities.splash.SplashActivity;
import groupdigital.com.paiementcie.Alert.AlertActivity;
import groupdigital.com.paiementcie.AutoLoad.AutoLoad;
import groupdigital.com.paiementcie.Content.Adapter.AdapterFacture;
import groupdigital.com.paiementcie.Content.Dto.FactureDto;
import groupdigital.com.paiementcie.Content.Dto.UserInformationDto;
import groupdigital.com.paiementcie.R;
import groupdigital.com.paiementcie.Rest.listener.ViewClickListener;
import groupdigital.com.paiementcie.Utilitaire.Route;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class UserInforamtionActivity extends AppCompatActivity implements ViewClickListener{
    private RecyclerView recyclerInvoice;
    private FastScroller fastInvoice;
    private Button btnPay;
    private Intent intent;
    private UserInformationDto userInformationDto;
    private Typeface Material, RobotoBold, RobotoLight;
    private ImageView avatar, pointLocation, makeLocation;
    private TextView nomPrenons, age, contact, montant_a_payer, title2, devise, gauch, titreHeader;
    private RelativeLayout ilYaImpayer, pasDimpayer;
    private AdapterFacture adapterFacture;
    private List<FactureDto> factureDtoList = new ArrayList<FactureDto>();
    private int totalSomme = 0;
    private List<FactureDto> choixFacture = new ArrayList<FactureDto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_inforamtion);

        ilYaImpayer = (RelativeLayout) findViewById(R.id.ilYaImpayer);
        pasDimpayer = (RelativeLayout) findViewById(R.id.pasDimpayer);

        Material = Typeface.createFromAsset(getAssets(), "fonts/Roboto-BoldCondensed.ttf");
        RobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
        RobotoLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        intent = getIntent();
        userInformationDto  = (UserInformationDto) intent.getSerializableExtra("data");

        titreHeader = (TextView)findViewById(R.id.titreHeader);
        title2 = (TextView)findViewById(R.id.title2);
        gauch = (TextView)findViewById(R.id.gauch);
        montant_a_payer = (TextView)findViewById(R.id.montant_a_payer);
        devise = (TextView)findViewById(R.id.devise);
        montant_a_payer.setText(""+totalSomme+"");
        pointLocation = (ImageView)findViewById(R.id.getLocation);
        makeLocation = (ImageView)findViewById(R.id.searchLocation);
        avatar = (ImageView)findViewById(R.id.avatar);
        nomPrenons = (TextView)findViewById(R.id.nomPrenons);
        age = (TextView)findViewById(R.id.age);
        contact = (TextView)findViewById(R.id.contact);


        if(String.valueOf(userInformationDto.getLatitude()) == "0.0" || String.valueOf(userInformationDto.getLongitude()) == "0.0"){
            makeLocation.setVisibility(View.VISIBLE);
            pointLocation.setVisibility(View.GONE);
        }

        else if(String.valueOf(userInformationDto.getLatitude()) != null || String.valueOf(userInformationDto.getLongitude()) != null){
            pointLocation.setVisibility(View.VISIBLE);
            makeLocation.setVisibility(View.GONE);
        }

        ImageView keyboard = (ImageView)findViewById(R.id.fermer_cmpte);
        keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnPay = (Button)findViewById(R.id._sign_pay);
        if(totalSomme == 0){
            btnPay.setVisibility(View.GONE);
        }
        btnPay.setTypeface(RobotoBold);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.i("OK--GHDD", String.valueOf(totalSomme));
                if(totalSomme == 0){
                    intent = new Intent(UserInforamtionActivity.this, AlertActivity.class);
                    intent.putExtra("msg", getResources().getString(R.string.action_bad));
                    intent.putExtra("key", "close");
                    startActivity(intent);
                }
                else{
                    intent = new Intent(UserInforamtionActivity.this, ModePayementActivity.class);
                    intent.putExtra("montantFacture", totalSomme);
                    intent.putExtra("choice", (Serializable) choixFacture);
                    intent.putExtra("infoUserDto", userInformationDto);
                    startActivity(intent);
                    //finish();
                }
            }
        });



        writeTo();
    }

    @SuppressLint("ResourceType")
    private void writeTo() {
        String photo = Route.IMAGE_PATH+"/"+userInformationDto.getPhoto();
        montant_a_payer.setTypeface(RobotoBold);
        devise.setTypeface(RobotoBold);
        gauch.setTypeface(RobotoBold);
        nomPrenons.setTypeface(RobotoLight);
        age.setTypeface(RobotoLight);
        title2.setTypeface(Material);
        titreHeader.setTypeface(Material);
        contact.setTypeface(RobotoLight);


        Glide.with(this)
            .load(photo)
            .placeholder(R.drawable.user_male_icon)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(avatar);
        nomPrenons.setText(getResources().getString(R.string.name)+" "+userInformationDto.getNom()+" "+userInformationDto.getPrenoms());
        age.setText(getResources().getString(R.string.age)+" "+userInformationDto.getAge() +" "+getResources().getString(R.string.ans));
        contact.setText(getResources().getString(R.string.contact)+" "+userInformationDto.getContact());


        makeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserInforamtionActivity.this, MapsOfTaxPayerActivity.class);
                intent.putExtra("taxpayerName", userInformationDto.getNom()+" "+userInformationDto.getPrenoms());
                intent.putExtra("ID", userInformationDto.getId());
                startActivity(intent);
            }
        });


        pointLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserInforamtionActivity.this, MapsActivity.class);
                intent.putExtra("getLat", userInformationDto.getLatitude());
                intent.putExtra("getLong", userInformationDto.getLongitude());
                intent.putExtra("taxpayerName", userInformationDto.getNom()+" "+userInformationDto.getPrenoms());
                startActivity(intent);
            }
        });

        initializeInvoice();
    }

    private void initializeInvoice() {
        recyclerInvoice = (RecyclerView) findViewById(R.id.listFacture);
        fastInvoice = (FastScroller) findViewById(R.id.fasterFacture);
        recyclerInvoice.setHasFixedSize(true);
        recyclerInvoice.setLayoutManager(new LinearLayoutManager(UserInforamtionActivity.this));
        adapterFacture = new AdapterFacture(this, factureDtoList, this);
        recyclerInvoice.setAdapter(adapterFacture);
        fastInvoice.setRecyclerView(recyclerInvoice);

        int sizeData = userInformationDto.getFactureDto().size();
        if(sizeData > 0){
            ilYaImpayer.setVisibility(View.VISIBLE);
            for(int i=0;i<sizeData;i++){
                factureDtoList.add(
                    new FactureDto(
                        userInformationDto.getFactureDto().get(i).getMontant(),
                        userInformationDto.getFactureDto().get(i).getDateFacture(),
                        userInformationDto.getFactureDto().get(i).getNumero()
                    )
                );
            }
            adapterFacture.notifyDataSetChanged();
        }
        else{
            pasDimpayer.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClicItemWithParam(int id, String action) {
        int montantCheck = 0, lastMount, newMount = 0;
        lastMount = Integer.parseInt(montant_a_payer.getText().toString());
        List<FactureDto> data = userInformationDto.getFactureDto();
        montantCheck += data.get(id).getMontant();
        if(action.equals("plus")){
            newMount += montantCheck + lastMount;
            choixFacture.add(data.get(id));
        }
        else if(action.equals("moins")){
            if(lastMount > 0){
                newMount = lastMount-montantCheck;
                int idObj = choixFacture.indexOf(data.get(id));
                choixFacture.remove(idObj);
            }
        }
        if(newMount > 0){
            btnPay.setVisibility(View.VISIBLE);
        }
        else{
            btnPay.setVisibility(View.GONE);
        }
        totalSomme = newMount;
        montant_a_payer.setText(""+newMount+"");
    }

    @Override
    public void onBackPressed() {
        AutoLoad.startNewActivity(UserInforamtionActivity.this, CollectActivity.class);
        finish();
    }



}
